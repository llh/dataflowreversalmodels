
PROGRAM MAIN
  USE SOLVER
  IMPLICIT NONE
  REAL*8, DIMENSION(0:N+1, 0:N+1) :: ru, rv, ru_prev, rv_prev, rdense, rdense_prev
  REAL*8 :: readu, readuprev, readv, readvprev, readdens, readdensprev
  REAL*8 :: dt=0.01D0
  REAL*8 :: visc=0.0001D0
  REAL*8 :: diff=0.0001D0
  REAL*8, DIMENSION(0:N+1) :: cu, cv
  REAL*8 :: ct
  REAL*8 :: cost, SIMULATE
  INTEGER :: i,j

  real :: startTime, endTime

  OPEN(10, file='../DATA')
  READ(10, *)
  DO j=0,N+1
     DO i=0,N+1
        READ(10, *) readu, readuprev, readv, readvprev, readdens, readdensprev
        ru(i,j) = readu
        rv(i,j) = readv
        ru_prev(i,j) = readuprev
        rv_prev(i,j) = readvprev
        rdense(i,j) = readdens
        rdense_prev(i,j) = readdensprev
     END DO
  END DO
  CLOSE(10)

  u = ru
  v = rv
  u_prev = ru_prev
  v_prev = rv_prev
  dense = rdense
  dense_prev = rdense_prev
  cu = 0.0D0
  cv = 0.0D0
  ct = 0.0D0

!  call CPU_TIME(startTime)
  cost = SIMULATE(dt, visc, diff, cu, cv, ct)
!  call CPU_TIME(endTime)

  PRINT *, '  (with F90) RAW RESULT: ', cost
!  PRINT '("Time of  primal: ",f6.3," seconds.")',endTime-startTime
     
END PROGRAM MAIN

REAL*8 FUNCTION SIMULATE(dt, visc, diff, cu, cv, ct)
  USE SOLVER
  IMPLICIT NONE
  REAL*8 :: dt, visc, diff
  REAL*8, DIMENSION(0:N+1) :: cu, cv
  REAL*8 :: ct
  CALL APPLYCONTROL(cu, cv)
  CALL TICKS(20, dt, visc, diff, ct)
  SIMULATE = COMPUTECOST()
END FUNCTION SIMULATE
