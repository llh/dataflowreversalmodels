MODULE SOLVER
  IMPLICIT NONE

  TYPE CHAINEDLIST
     REAL*8 :: head
     TYPE(CHAINEDLIST), POINTER :: tail
  END TYPE CHAINEDLIST

  INTEGER,PARAMETER :: N=80
  REAL*8, TARGET, DIMENSION(0:N+1,0:N+1) :: u, v, u_prev, v_prev, dense, dense_prev
  TYPE(CHAINEDLIST), POINTER :: timecontrols
  REAL*8, POINTER, DIMENSION(:,:) :: argx, argx0

CONTAINS

  !$AD NOCHECKPOINT
  SUBROUTINE APPLYCONTROL(du, dv)
    REAL*8, DIMENSION(0:N+1) :: du,dv
    u(30,0:N+1) = u(30,0:N+1) + du(0:N+1)
    v(30,0:N+1) = v(30,0:N+1) + dv(0:N+1)
  END SUBROUTINE APPLYCONTROL

  REAL*8 FUNCTION COMPUTECOST()
    COMPUTECOST = SUM(u(0:N+1,70)*u(0:N+1,70) + v(0:N+1,70)*v(0:N+1,70))
  END FUNCTION COMPUTECOST

  !$AD NOCHECKPOINT
  SUBROUTINE TICKS(nbticks, dt, visc, diff, ct)
    INTEGER :: nbticks, i
    REAL*8 :: dt, visc, diff, ct
    TYPE(CHAINEDLIST), POINTER :: newtimecontrols
    timecontrols => NULL()
    DO i=1,nbticks
       ALLOCATE(newtimecontrols)
       newtimecontrols%head = ct
       newtimecontrols%tail => timecontrols
       timecontrols => newtimecontrols
    END DO
    DO i=1,nbticks
       CALL TICK(dt, visc, diff)
    END DO
  END SUBROUTINE TICKS

  !$AD NOCHECKPOINT
  SUBROUTINE TICK(dt, visc, diff)
    REAL*8 :: dt, visc, diff
    CALL VEL_STEP(u, v, u_prev, v_prev, visc, dt);
    CALL DENS_STEP(dense, dense_prev, u, v, diff, dt);
  END SUBROUTINE TICK

  !$AD NOCHECKPOINT
  SUBROUTINE VEL_STEP(u, v, u0, v0, visc, dt)
    REAL*8, TARGET, DIMENSION(0:N+1,0:N+1) :: u, v, u0, v0
    REAL*8, POINTER, DIMENSION(:,:) :: argu1, argu2, argv1, argv2
    REAL*8 :: visc, dt
    u(40,40) = u(40,40) + timecontrols%head
    timecontrols => timecontrols%tail
    CALL ADD_SOURCE(u, u0, dt)
    CALL ADD_SOURCE(v, v0, dt)
    argx => u0
    argx0 => u
    CALL DIFFUSE(1, visc, dt)
    argx => v0
    argx0 => v
    CALL DIFFUSE(2, visc, dt)
    argu1 => u0
    argu2 => u
    argv1 => v0
    argv2 => v
    CALL PROJECT(argu1, argv1, argu2, argv2)
    CALL ADVECT(1, u, u0, u0, v0, dt)
    CALL ADVECT(2, v, v0, u0, v0, dt)
    CALL PROJECT(u, v, u0, v0)
  END SUBROUTINE VEL_STEP

  !$AD NOCHECKPOINT
  SUBROUTINE DENS_STEP(x, x0, u, v, diff, dt)
    REAL*8, TARGET, DIMENSION(0:N+1,0:N+1) :: x, x0, u, v
    REAL*8 :: diff, dt
    argx => x0
    argx0 => x
    CALL DIFFUSE(0, diff, dt)
    argx => x
    argx0 => x0
    CALL DIFFUSE(0, diff, dt)
  END SUBROUTINE DENS_STEP

  !$AD NOCHECKPOINT
  SUBROUTINE ADD_SOURCE(x, s, dt)
    REAL*8, DIMENSION(0:N+1,0:N+1) :: x, s
    REAL*8 :: dt
    x = x + dt*s
  END SUBROUTINE ADD_SOURCE

  !$AD NOCHECKPOINT
  SUBROUTINE DIFFUSE(b, diff, dt)
    INTEGER :: b
    REAL*8 :: diff, dt
    INTEGER :: i,j,k
    REAL*8 :: a
    a = dt*diff*N*N
    DO k = 0,19
       DO i = 1,N
          DO j = 1,N
             argx(i,j) = (argx0(i,j)  &
&                       + a*(argx(i-1,j)+argx(i+1,j)+argx(i,j-1)+argx(i,j+1))) &
&                      / (1.0D0 + 4.0D0*a)
          END DO
       END DO
       CALL SET_BND(b, argx)
    END DO
  END SUBROUTINE DIFFUSE

  !$AD NOCHECKPOINT
  SUBROUTINE ADVECT(b, d, d0, u, v, dt)
    INTEGER :: b
    REAL*8, DIMENSION(0:N+1,0:N+1) :: d, d0, u, v
    REAL*8 :: dt
    INTEGER :: i, j, i0, j0, i1, j1
    REAL*8 :: x, y, s0, t0, s1, t1, dt0
    dt0 = dt*N
    DO i=1,N
       DO j=1,N
          x = i - dt0*u(i,j)
          y = j - dt0*v(i,j)
          x = MIN(MAX(x, 0.5D0), N+0.5)
          i0 = FLOOR(x)
          i1 = i0 + 1
          y = MIN(MAX(y, 0.5D0), N+0.5)
          j0 = FLOOR(y)
          j1 = j0 + 1
          s1 = x - i0
          s0 = 1 - s1
          t1 = y - j0
          t0 = 1 - t1
          d(i,j) = s0 * (t0*d0(i0,j0) + t1*d0(i0,j1))   &
&                + s1 * (t0*d0(i1,j0) + t1*d0(i1,j1))
       END DO
    END DO
    CALL SET_BND(b,d)
  END SUBROUTINE ADVECT

  !$AD NOCHECKPOINT
  SUBROUTINE PROJECT(u, v, p, div)
    REAL*8, DIMENSION(0:N+1,0:N+1) :: u, v, p, div
    INTEGER :: i,j,k
    REAL*8 :: h
    h = 1.0D0/N
    div(1:N,1:N) = -0.5D0*h*(u(2:(N+1),1:N) - u(0:(N-1),1:N) + v(1:N,2:(N+1)) - v(1:N,0:(N-1)))
    p(1:N,1:N) = 0.0D0
    CALL SET_BND(0,div)
    CALL SET_BND(0,p)
    DO k = 0,19
       DO i = 1,N
          DO j = 1,N
             p(i,j) = (div(i,j) + p(i-1,j) + p(i+1,j) + p(i,j-1) +p(i,j+1)) / 4.0D0
          END DO
       END DO
       CALL SET_BND(0,p)
    END DO
    u(1:N,1:N) = u(1:N,1:N) - 0.5D0*(p(2:(N+1),1:N) - p(0:(N-1),1:N))/h
    v(1:N,1:N) = v(1:N,1:N) - 0.5D0*(p(1:N,2:(N+1)) - p(1:N,0:(N-1)))/h
    CALL SET_BND(1,u)
    CALL SET_BND(2,v)
  END SUBROUTINE PROJECT
    
  !$AD NOCHECKPOINT
  SUBROUTINE SET_BND(b, x)
    INTEGER :: b
    REAL*8, DIMENSION(0:N+1,0:N+1) :: x
    INTEGER :: i
    DO i=1,N
       IF (b.eq.1) THEN
          x(0,i) = -x(1,i)
       ELSE
          x(0,i) = x(1,i)
       END IF
       IF (b.eq.1) THEN
          x(N+1,i) = -x(N,i)
       ELSE
          x(N+1,i) = x(N,i)
       END IF
       IF (b.eq.2) THEN
          x(i,0) = -x(i,1)
       ELSE
          x(i,0) = x(i,1)
       END IF
       IF (b.eq.2) THEN
          x(i,N+1) = -x(i,N)
       ELSE
          x(i,N+1) = x(i,N)
       END IF
    END DO
    x(0,0) = 0.5D0 * (x(1,0) + x(0,1))
    x(0,N+1) = 0.5D0 * (x(1,N+1) + x(0,N))
    x(N+1,0) = 0.5D0 * (x(N,0) + x(N+1,1))
    x(N+1,N+1) = 0.5D0 *(x(N,N+1) + x(N+1,N))
  END SUBROUTINE SET_BND

END MODULE SOLVER
