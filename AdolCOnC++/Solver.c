#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <adolc/adouble.h>
#include "Solver.h"

int N ;
int SIZE ;
adouble *u ;
adouble *v ;
adouble *u_prev ;
adouble *v_prev ;
adouble *dense ;
adouble *dense_prev ;
ChainedList *timeControls ;
adouble *argx ;
adouble *argx0 ;

void applyControl(adouble *du, adouble *dv) {
  for (int i=0 ; i<N+2 ; ++i) {
    int index = INDEX(30,i) ;
    u[index] += du[i] ;
    v[index] += dv[i] ;
  }
}

adouble computeCost() {
  adouble cost = 0.0 ;
  for (int i=0 ; i<N+2 ; ++i) {
    int index = INDEX(i,70) ;
    cost += u[index]*u[index] + v[index]*v[index] ;
  }
  return cost ;
}

void ticks(int nbTicks, double dt, double visc, double diff, adouble ct) {
  timeControls = NULL ;
  ChainedList *newtimeControls ;
  for (int i=1 ; i<=nbTicks ; ++i) {
    newtimeControls = new ChainedList;
    newtimeControls->head = ct ;
    newtimeControls->tail = timeControls ;
    timeControls = newtimeControls ;
  }
  for (int i=1 ; i<=nbTicks ; ++i) {
    tick(dt, visc, diff) ;
  }
}

void tick(double dt, double visc, double diff) {
  vel_step(u, v, u_prev, v_prev, visc, dt);
  dens_step(dense, dense_prev, u, v, diff, dt);
}

void vel_step(adouble* u, adouble* v, adouble* u0, adouble* v0,
              double visc, double dt) {
  u[INDEX(40,40)] += timeControls->head ;
  timeControls = timeControls->tail ;
  add_source(u, u0, dt);
  add_source(v, v0, dt);

  {
    argx = u0 ;
    argx0 = u ;
    diffuse(1, visc, dt);
    argx = v0 ;
    argx0 = v ;
    diffuse(2, visc, dt);
    adouble* argu1 = u0 ;
    adouble* argu2 = u ;
    adouble* argv1 = v0 ;
    adouble* argv2 = v ;
    project(argu1, argv1, argu2, argv2);
  }

  advect(1, u, u0, u0, v0, dt);
  advect(2, v, v0, u0, v0, dt);
  project(u, v, u0, v0);
}

void dens_step(adouble* x, adouble* x0, adouble* u, adouble* v,
               double diff, double dt) {
  argx = x0 ;
  argx0 = x ;
  diffuse(0, diff, dt);
  argx = x ;
  argx0 = x0 ;
  diffuse(0, diff, dt);
}

void add_source(adouble* x, adouble* s, double dt) {
  int i, size = (N + 2) * (N + 2);
  for (i = 0; i < size; i++)
    x[i] += dt * s[i];
}

void diffuse(int b, double diff, double dt) {
  int i, j, k;
  double a = dt * diff * N * N;
  for (k = 0; k < 20; k++) {
    for (i = 1; i <= N; i++) {
      for (j = 1; j <= N; j++) {
        argx[INDEX(i, j)] = (argx0[INDEX(i, j)] + a
                             * (argx[INDEX(i - 1, j)] + argx[INDEX(i + 1, j)]
                                + argx[INDEX(i, j - 1)] + argx[INDEX(i, j + 1)]))
          / (1 + 4 * a);
      }
    }
    set_bnd(b, argx);
  }
}

void advect(int b, adouble* d, adouble* d0, adouble* u, adouble* v, double dt) {
  int i, j, i0, j0, i1, j1;
  adouble x, y, s0, t0, s1, t1 ;
  double dt0;
  dt0 = dt * N;
  for (i = 1; i <= N; i++) {
    for (j = 1; j <= N; j++) {
      x = i - dt0 * u[INDEX(i, j)];
      y = j - dt0 * v[INDEX(i, j)];
      if (x < 0.5)
        x = 0.5;
      if (x > N + 0.5)
        x = N + 0.5;
      i0 = (int) x.value();
      i1 = i0 + 1;
      if (y < 0.5)
        y = 0.5;
      if (y > N + 0.5)
        y = N + 0.5;
      j0 = (int) y.value();
      j1 = j0 + 1;
      s1 = x - i0;
      s0 = 1 - s1;
      t1 = y - j0;
      t0 = 1 - t1;
      d[INDEX(i, j)] = s0 * (t0 * d0[INDEX(i0, j0)] + t1 * d0[INDEX(i0, j1)])
        + s1 * (t0 * d0[INDEX(i1, j0)] + t1 * d0[INDEX(i1, j1)]);
    }
  }
  set_bnd(b, d);
}

void project(adouble* u, adouble* v, adouble* p, adouble* div) {
  int i, j, k;
  double h;
  h = 1.0 / N;
  for (i = 1; i <= N; i++) {
    for (j = 1; j <= N; j++) {
      div[INDEX(i, j)] = -0.5
        * h
        * (u[INDEX(i + 1, j)] - u[INDEX(i - 1, j)] + v[INDEX(i, j + 1)] - v[INDEX(
                                                                                  i, j - 1)]);
      p[INDEX(i, j)] = 0;
    }
  }
  set_bnd(0, div);
  set_bnd(0, p);
  for (k = 0; k < 20; k++) {
    for (i = 1; i <= N; i++) {
      for (j = 1; j <= N; j++) {
        p[INDEX(i, j)] = (div[INDEX(i, j)] + p[INDEX(i - 1, j)]
                          + p[INDEX(i + 1, j)] + p[INDEX(i, j - 1)] + p[INDEX(i, j + 1)]) / 4;
      }
    }
    set_bnd(0, p);
  }
  for (i = 1; i <= N; i++) {
    for (j = 1; j <= N; j++) {
      u[INDEX(i, j)] -= 0.5 * (p[INDEX(i + 1, j)] - p[INDEX(i - 1, j)]) / h;
      v[INDEX(i, j)] -= 0.5 * (p[INDEX(i, j + 1)] - p[INDEX(i, j - 1)]) / h;
    }
  }
  set_bnd(1, u);
  set_bnd(2, v);
}

void set_bnd(int b, adouble* x) {
  int i;
  for (i = 1; i <= N; i++) {
    x[INDEX(0, i)] = (b == 1) ? -x[INDEX(1, i)] : x[INDEX(1, i)];
    x[INDEX(N + 1, i)] = b == 1 ? -x[INDEX(N, i)] : x[INDEX(N, i)];
    x[INDEX(i, 0)] = b == 2 ? -x[INDEX(i, 1)] : x[INDEX(i, 1)];
    x[INDEX(i, N + 1)] = b == 2 ? -x[INDEX(i, N)] : x[INDEX(i, N)];
  }
  x[INDEX(0, 0)] = 0.5 * (x[INDEX(1, 0)] + x[INDEX(0, 1)]);
  x[INDEX(0, N + 1)] = 0.5 * (x[INDEX(1, N + 1)] + x[INDEX(0, N)]);
  x[INDEX(N + 1, 0)] = 0.5 * (x[INDEX(N, 0)] + x[INDEX(N + 1, 1)]);
  x[INDEX(N + 1, N + 1)] = 0.5 * (x[INDEX(N, N + 1)] + x[INDEX(N + 1, N)]);
}

int INDEX(int i, int j) {
  return i + (N + 2) * j;
}
