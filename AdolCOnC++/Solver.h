#include <stddef.h>

typedef struct _ChainedList{
  adouble head ;
  struct _ChainedList *tail ;
} ChainedList ;

extern void applyControl(adouble *du, adouble *dv) ;

extern adouble computeCost() ;

extern void ticks(int nbTicks, double dt, double visc, double diff, adouble ct) ;

extern void tick(double dt, double visc, double diff) ;

extern void vel_step(adouble* u, adouble* v, adouble* u0, adouble* v0,
              double visc, double dt) ;

extern void dens_step(adouble* x, adouble* x0, adouble* u, adouble* v,
               double diff, double dt) ;

extern void add_source(adouble* x, adouble* s, double dt) ;

extern void diffuse(int b, double diff, double dt) ;

extern void advect(int b, adouble* d, adouble* d0, adouble* u, adouble* v, double dt) ;

extern void project(adouble* u, adouble* v, adouble* p, adouble* div) ;

extern void set_bnd(int b, adouble* x) ;

extern int INDEX(int i, int j) ;
