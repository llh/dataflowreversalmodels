#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <adolc/adouble.h>
#include <adolc/interfaces.h> 
#include <adolc/drivers/drivers.h> 
#include <adolc/taping.h>
#include "Solver.h"

extern int N ;
extern int SIZE ;
extern adouble *u ;
extern adouble *v ;
extern adouble *u_prev ;
extern adouble *v_prev ;
extern adouble *dense ;
extern adouble *dense_prev ;
extern ChainedList *timeControls ;

adouble simulate(double dt, double visc, double diff,
                adouble *cu, adouble *cv, adouble ct) ;

// For condensing derivatives into one scalar:
double seed = 0.87 ;
double curSeed = 0.0 ;
double nextCurSeed() {
  curSeed += seed ;
  if (curSeed>=1.0) curSeed-=1.0 ;
  return curSeed+1.0 ;
}

int main(int argc, char *argv[]) {
  int i ;
  N = 80 ;
  SIZE = (N + 2) * (N + 2) ;

  clock_t startTime, endTime ;

  double* ru = (double*)malloc(SIZE*sizeof(double)) ;
  double* rv = (double*)malloc(SIZE*sizeof(double)) ;
  double* ru_prev = (double*)malloc(SIZE*sizeof(double)) ;
  double* rv_prev = (double*)malloc(SIZE*sizeof(double)) ;
  double* rdense = (double*)malloc(SIZE*sizeof(double)) ;
  double* rdense_prev = (double*)malloc(SIZE*sizeof(double)) ;
  double readu, readuprev, readv, readvprev, readdens, readdensprev ;
  FILE* datafile = fopen("../DATA", "r") ;
  char *skippedLine = (char*)malloc(80) ;
  fscanf(datafile, "%[^\n]\n", skippedLine) ;   // dt visc diff
  i = 0 ;
  int status = 1 ;
  while (status != EOF) {
    status = fscanf(datafile, "%lf %lf %lf %lf %lf %lf\n",
                    &readu, &readuprev, &readv, &readvprev,
                    &readdens, &readdensprev) ;
    if (status != EOF) {
      ru[i] = readu ;
      rv[i] = readv ;
      ru_prev[i] = readuprev ;
      rv_prev[i] = readvprev ;
      rdense[i] = readdens ;
      rdense_prev[i] = readdensprev ;
      ++i ;
    }
  }
  fclose(datafile) ;

  u = new adouble[SIZE];
  v = new adouble[SIZE];
  u_prev = new adouble[SIZE];
  v_prev = new adouble[SIZE];
  dense = new adouble[SIZE];
  dense_prev = new adouble[SIZE];

  double dt=0.01, visc=0.0001, diff=0.0001;
  adouble cu[N+2] ;
  adouble cv[N+2] ;
  adouble ct ;

  trace_on(1) ;
  for (i=0 ; i<SIZE ; ++i) {
    u[i] = ru[i];
    v[i] = rv[i];
    u_prev[i] = ru_prev[i];
    v_prev[i] = rv_prev[i];
    dense[i] = rdense[i];
    dense_prev[i] = rdense_prev[i];
  }
  ct <<= 0.0 ;
  for (i=0 ; i<N+2 ; ++i) {
    cu[i] <<= 0.0 ;
  }
  for (i=0 ; i<N+2 ; ++i) {
    cv[i] <<= 0.0 ;
  }
  adouble acost ;
  startTime = clock() ;
  acost = simulate(dt, visc, diff, cu, cv, ct) ;
  endTime = clock() ;
  double cost ;
  acost >>= cost ;
  trace_off() ;

  printf("  (with C++ AdolC) RAW RESULT: %18.15f\n", cost) ;
  printf("  Time of fwd record: %lf s.\n", (double)(endTime-startTime)/CLOCKS_PER_SEC) ;

  double inputForAdolC[2*N+5] ;
  double inputDForAdolC[2*N+5] ;
  double inputBFromAdolC[2*N+5] ;
  double outputFromAdolC[1] ;
  double outputDFromAdolC[1] ;
  double outputBForAdolC[1] ;
  double condens ;

  // TANGENT MODE
  for (i=0 ; i<2*N+5 ; ++i) inputForAdolC[i] = 0.0 ;
  curSeed = 0.0 ;
  inputDForAdolC[0] = nextCurSeed() ;
  for (i=1 ; i<N+3 ; ++i) {
    inputDForAdolC[i] = nextCurSeed() ;
  }
  for (i=N+3 ; i<2*N+5 ; ++i) {
    inputDForAdolC[i] = nextCurSeed() ;
  }
  startTime = clock() ;
  fos_forward(1,1,2*N+5,0,inputForAdolC, inputDForAdolC, outputFromAdolC, outputDFromAdolC) ;
  endTime = clock() ;
  printf("  Time of fos_forward: %lf s.\n", (double)(endTime-startTime)/CLOCKS_PER_SEC) ;
  curSeed = 0.0 ;
  condens = outputDFromAdolC[0]*nextCurSeed() ;
  printf("[seed:%6.3f] Condensed tangent: %18.15f\n", seed, condens) ;

  startTime = clock() ;
  zos_forward(1,1,2*N+5,1,inputForAdolC, outputFromAdolC) ;
  endTime = clock() ;
  printf("  Time of zos_forward: %lf s.\n", (double)(endTime-startTime)/CLOCKS_PER_SEC) ;

  // GRADIENT MODE
  for (i=0 ; i<2*N+5 ; ++i) inputForAdolC[i] = 0.0 ;
  outputBForAdolC[0] = 1.0 ;
  startTime = clock() ;
  fos_reverse(1,1,2*N+5,outputBForAdolC, inputBFromAdolC);
  endTime = clock() ;
  printf("  Time of bwd gradient: %lf s.\n", (double)(endTime-startTime)/CLOCKS_PER_SEC) ;
  curSeed = 0.0 ;
  condens = 0.0 ;
  condens += inputBFromAdolC[0]*nextCurSeed() ;
  for (i=1 ; i<N+3 ; ++i) {
    condens += inputBFromAdolC[i]*nextCurSeed() ;
  }
  for (i=N+3 ; i<2*N+5 ; ++i) {
    condens += inputBFromAdolC[i]*nextCurSeed() ;
  }
  curSeed = 0.0 ;
  condens = condens*nextCurSeed() ;
  printf("[seed:%6.3f] Condensed gradient: %18.15f\n", seed, condens);
}

adouble simulate(double dt, double visc, double diff,
                adouble *cu, adouble *cv, adouble ct) {
  applyControl(cu, cv) ;
  ticks(20, dt, visc, diff, ct) ;
  return computeCost() ;
}
