/** A pair of a primal variable and its tangent derivative, in Association-by-Address fashion.
 * This is equivalent to the classical aDouble */
public class Double_D {
    public double val ;
    public double tgt = 0.0 ;

    public Double_D() {
        this.val = 0.0 ;
        this.tgt = 0.0 ;
    }

    public Double_D(double val) {
        this.val = val ;
        this.tgt = 0.0 ;
    }

    public Double_D(double val, double tgt) {
        this.val = val ;
        this.tgt = tgt ;
    }
}
