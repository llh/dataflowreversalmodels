
/** Tangent AD of Class Solver. */
public class Solver_D {
    int N ;
    int SIZE ;
    DoubleArray_D u ;
    DoubleArray_D v ;
    DoubleArray_D u_prev ;
    DoubleArray_D v_prev ;
    DoubleArray_D dense ;
    DoubleArray_D dense_prev ;
    ChainedList_D timeControls ;
    DoubleArray_D argx ;
    DoubleArray_D argx0 ;

    public Solver_D(int N) {
        this.N = N;
        SIZE = (N + 2) * (N + 2);
        u = new DoubleArray_D(SIZE) ;
        v = new DoubleArray_D(SIZE) ;
        u_prev = new DoubleArray_D(SIZE) ;
        v_prev = new DoubleArray_D(SIZE) ;
        dense = new DoubleArray_D(SIZE) ;
        dense_prev = new DoubleArray_D(SIZE) ;
    }

    public void applyControl(DoubleArray_D du, DoubleArray_D dv) {
        int ii2 = 82, jj2 = 30 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = N/2 ;
        }
        for (int i=0 ; i<ii2 ; ++i) {
            int index = INDEX(jj2,i) ;
            u.vals[index] += du.vals[i] ;
            v.vals[index] += dv.vals[i] ;
        }
    }

    public void applyControl_d(DoubleArray_D du, DoubleArray_D dv) {
        int ii2 = 82, jj2 = 30 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = N/2 ;
        }
        for (int i=0 ; i<ii2 ; ++i) {
            int index = INDEX(jj2,i) ;
            u.tgts[index] += du.tgts[i] ;
            u.vals[index] += du.vals[i] ;
            v.tgts[index] += dv.tgts[i] ;
            v.vals[index] += dv.vals[i] ;
        }
    }

    public double computeCost() {
        int ii2 = 82, jj2 = 70 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = (3*N)/4 ;
        }
        double cost = 0.0 ;
        for (int i=0 ; i<ii2 ; ++i) {
            int index = INDEX(i,jj2) ;
            cost += u.vals[index]*u.vals[index] + v.vals[index]*v.vals[index] ;
        }
        return cost ;
    }

    public double computeCost_d(Todouble computeCost) {
        int ii2 = 82, jj2 = 70 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = (3*N)/4 ;
        }
        double costd = 0.0 ;
        double cost = 0.0 ;
        for (int i=0 ; i<ii2 ; ++i) {
            int index = INDEX(i,jj2) ;
            costd += 2.0*u.vals[index]*u.tgts[index] + 2.0*v.vals[index]*v.tgts[index] ;
            cost += u.vals[index]*u.vals[index] + v.vals[index]*v.vals[index] ;
        }
        computeCost.set(cost) ;

        return costd ;
    }

    public void ticks(int nbTicks, double dt, double visc, double diff, double ct) {
        timeControls = null ;
        for (int i=1 ; i<=nbTicks ; ++i) {
            timeControls = new ChainedList_D(new Double_D(ct), timeControls) ;
        }
        for (int i=1 ; i<=nbTicks ; ++i) {
            tick(dt, visc, diff) ;
        }
    }

    public void ticks_d(int nbTicks, double dt, double visc, double diff, double ct, double ctd) {
        timeControls = null ;
        for (int i=1 ; i<=nbTicks ; ++i) {
            timeControls = new ChainedList_D(new Double_D(ct, ctd), timeControls) ;
        }
        for (int i=1 ; i<=nbTicks ; ++i) {
            tick_d(dt, visc, diff) ;
// System.out.println("********************************************* TGT TICK "+i) ;
        }
    }

    void tick(double dt, double visc, double diff) {
        vel_step(u, v, u_prev, v_prev, visc, dt);
        dens_step(dense, dense_prev, u, v, diff, dt);
    }

    void tick_d(double dt, double visc, double diff) {
        vel_step_d(u, v, u_prev, v_prev, visc, dt);
        dens_step_d(dense, dense_prev, u, v, diff, dt);
    }

    void vel_step(DoubleArray_D u, DoubleArray_D v, DoubleArray_D u0, DoubleArray_D v0, double visc,
                  double dt) {
        int ii2 = 40 ;
        if (N<80) {
            ii2 = N/2 ;
        }
        u.vals[INDEX(ii2,ii2)] += ((Double_D)timeControls.head).val ;
        timeControls = timeControls.tail ;
        add_source(u, u0, dt);
        add_source(v, v0, dt);

        {
            argx = u0 ;
            argx0 = u ;
            diffuse(1, visc, dt);
            argx = v0 ;
            argx0 = v ;
            diffuse(2, visc, dt);
            DoubleArray_D argu1 = u0 ;
            DoubleArray_D argu2 = u ;
            DoubleArray_D argv1 = v0 ;
            DoubleArray_D argv2 = v ;
            project(argu1, argv1, argu2, argv2);
        }

        advect(1, u, u0, u0, v0, dt);
        advect(2, v, v0, u0, v0, dt);
        project(u, v, u0, v0);

    }

    void vel_step_d(DoubleArray_D u, DoubleArray_D v, DoubleArray_D u0, DoubleArray_D v0, double visc,
                  double dt) {
        int ii2 = 40 ;
        if (N<80) {
            ii2 = N/2 ;
        }
        u.tgts[INDEX(ii2,ii2)] += ((Double_D)timeControls.head).tgt ;
        u.vals[INDEX(ii2,ii2)] += ((Double_D)timeControls.head).val ;
        timeControls = timeControls.tail ;
        add_source_d(u, u0, dt);
        add_source_d(v, v0, dt);
        {
            argx = u0 ;
            argx0 = u ;
            diffuse_d(1, visc, dt);
            argx = v0 ;
            argx0 = v ;
            diffuse_d(2, visc, dt);
            DoubleArray_D argu1 = u0 ;
            DoubleArray_D argu2 = u ;
            DoubleArray_D argv1 = v0 ;
            DoubleArray_D argv2 = v ;
            project_d(argu1, argv1, argu2, argv2);
        }
        advect_d(1, u, u0, u0, v0, dt);
        advect_d(2, v, v0, u0, v0, dt);
        project_d(u, v, u0, v0);
    }

    void dens_step(DoubleArray_D x, DoubleArray_D x0, DoubleArray_D u, DoubleArray_D v, double diff,
            double dt) {
        argx = x0 ;
        argx0 = x ;
        diffuse(0, diff, dt);
        argx = x ;
        argx0 = x0 ;
        diffuse(0, diff, dt);
    }

    void dens_step_d(DoubleArray_D x, DoubleArray_D x0, DoubleArray_D u, DoubleArray_D v, double diff,
            double dt) {
        argx = x0 ;
        argx0 = x ;
        diffuse_d(0, diff, dt);
        argx = x ;
        argx0 = x0 ;
        diffuse_d(0, diff, dt);
    }

    void add_source(DoubleArray_D x, DoubleArray_D s, double dt) {
        int i, size = (N + 2) * (N + 2);
        for (i = 0; i < size; i++)
            x.vals[i] += dt * s.vals[i] ;
    }

    void add_source_d(DoubleArray_D x, DoubleArray_D s, double dt) {
        int i, size = (N + 2) * (N + 2);
        for (i = 0; i < size; i++) {
            x.tgts[i] += dt * s.tgts[i] ;
            x.vals[i] += dt * s.vals[i] ;
        }
    }

    void diffuse(int b, double diff, double dt) {
        int i, j, k;
        double a = dt * diff * N * N;
        for (k = 0; k < 20; k++) {
            for (i = 1; i <= N; i++) {
                for (j = 1; j <= N; j++) {
                    argx.vals[INDEX(i, j)] =
                        (argx0.vals[INDEX(i, j)]
                         + a * (argx.vals[INDEX(i - 1, j)] 
                                + argx.vals[INDEX(i + 1, j)]
                                + argx.vals[INDEX(i, j - 1)]
                                + argx.vals[INDEX(i, j + 1)]))
                        / (1 + 4 * a) ;
                }
            }
            set_bnd(b, argx);
        }
    }

    void diffuse_d(int b, double diff, double dt) {
        int i, j, k;
        double a = dt * diff * N * N;
        for (k = 0; k < 20; k++) {
            for (i = 1; i <= N; i++) {
                for (j = 1; j <= N; j++) {
                    argx.tgts[INDEX(i, j)] =
                        (argx0.tgts[INDEX(i, j)]
                         + a * (argx.tgts[INDEX(i - 1, j)] 
                                + argx.tgts[INDEX(i + 1, j)]
                                + argx.tgts[INDEX(i, j - 1)]
                                + argx.tgts[INDEX(i, j + 1)]))
                        / (1 + 4 * a) ;
                    argx.vals[INDEX(i, j)] =
                        (argx0.vals[INDEX(i, j)]
                         + a * (argx.vals[INDEX(i - 1, j)] 
                                + argx.vals[INDEX(i + 1, j)]
                                + argx.vals[INDEX(i, j - 1)]
                                + argx.vals[INDEX(i, j + 1)]))
                        / (1 + 4 * a) ;
                }
            }
            set_bnd_d(b, argx);
        }
    }

    void advect(int b, DoubleArray_D d, DoubleArray_D d0, DoubleArray_D u, DoubleArray_D v, double dt) {
        int i, j, i0, j0, i1, j1;
        double x, y, s0, t0, s1, t1, dt0;
        dt0 = dt * N;
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                x = i - dt0 * u.vals[INDEX(i, j)];
                y = j - dt0 * v.vals[INDEX(i, j)];
                if (x < 0.5)
                    x = 0.5;
                if (x > N + 0.5)
                    x = N + 0.5;
                i0 = (int) x;
                i1 = i0 + 1;
                if (y < 0.5)
                    y = 0.5;
                if (y > N + 0.5)
                    y = N + 0.5;
                j0 = (int) y;
                j1 = j0 + 1;
                s1 = x - i0;
                s0 = 1 - s1;
                t1 = y - j0;
                t0 = 1 - t1;
                d.vals[INDEX(i, j)] =
                    s0 * (t0 * d0.vals[INDEX(i0, j0)] + t1 * d0.vals[INDEX(i0, j1)])
                    + s1 * (t0 * d0.vals[INDEX(i1, j0)] + t1 * d0.vals[INDEX(i1, j1)]);
            }
        }
        set_bnd(b, d);
    }

    void advect_d(int b, DoubleArray_D d, DoubleArray_D d0, DoubleArray_D u, DoubleArray_D v, double dt) {
        int i, j, i0, j0, i1, j1;
        double x, y, s0, t0, s1, t1, dt0;
        double xd, yd, s0d, t0d, s1d, t1d ;
        dt0 = dt * N;
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                xd = - dt0 * u.tgts[INDEX(i, j)];
                x = i - dt0 * u.vals[INDEX(i, j)];
                yd = - dt0 * v.tgts[INDEX(i, j)];
                y = j - dt0 * v.vals[INDEX(i, j)];
                if (x < 0.5) {
                    xd = 0.0 ;
                    x = 0.5;
                }
                if (x > N + 0.5) {
                    xd = 0.0 ;
                    x = N + 0.5;
                }
                i0 = (int) x;
                i1 = i0 + 1;
                if (y < 0.5) {
                    yd = 0.0 ;
                    y = 0.5;
                }
                if (y > N + 0.5) {
                    yd = 0.0 ;
                    y = N + 0.5;
                }
                j0 = (int) y;
                j1 = j0 + 1;
                s1d = xd;
                s1 = x - i0;
                s0d = -s1d;
                s0 = 1 - s1;
                t1d = yd;
                t1 = y - j0;
                t0d = - t1d;
                t0 = 1 - t1;
                d.tgts[INDEX(i, j)] =
                    s0d * (t0 * d0.vals[INDEX(i0, j0)] + t1 * d0.vals[INDEX(i0, j1)])
                    + s0 * (t0d * d0.vals[INDEX(i0, j0)]
                            + t0 * d0.tgts[INDEX(i0, j0)]
                            + t1d * d0.vals[INDEX(i0, j1)]
                            + t1 * d0.tgts[INDEX(i0, j1)])
                    + s1d * (t0 * d0.vals[INDEX(i1, j0)] + t1 * d0.vals[INDEX(i1, j1)])
                    + s1 * (t0d * d0.vals[INDEX(i1, j0)]
                            + t0 * d0.tgts[INDEX(i1, j0)]
                            + t1d * d0.vals[INDEX(i1, j1)]
                            + t1 * d0.tgts[INDEX(i1, j1)]);
                d.vals[INDEX(i, j)] =
                    s0 * (t0 * d0.vals[INDEX(i0, j0)] + t1 * d0.vals[INDEX(i0, j1)])
                    + s1 * (t0 * d0.vals[INDEX(i1, j0)] + t1 * d0.vals[INDEX(i1, j1)]);
            }
        }
        set_bnd_d(b, d);
    }

    void project(DoubleArray_D u, DoubleArray_D v, DoubleArray_D p, DoubleArray_D div) {
        int i, j, k;
        double h;
        h = 1.0 / N;
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                div.vals[INDEX(i, j)] = -0.5 * h *
                    (u.vals[INDEX(i + 1, j)]
                     - u.vals[INDEX(i - 1, j)]
                     + v.vals[INDEX(i, j + 1)]
                     - v.vals[INDEX(i, j - 1)]) ;
                p.vals[INDEX(i, j)] = 0;
            }
        }
        set_bnd(0, div);
        set_bnd(0, p);
        for (k = 0; k < 20; k++) {
            for (i = 1; i <= N; i++) {
                for (j = 1; j <= N; j++) {
                    p.vals[INDEX(i, j)] =
                        (div.vals[INDEX(i, j)]
                         + p.vals[INDEX(i - 1, j)]
                         + p.vals[INDEX(i + 1, j)]
                         + p.vals[INDEX(i, j - 1)]
                         + p.vals[INDEX(i, j + 1)]) / 4;
                }
            }
            set_bnd(0, p);
        }
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                u.vals[INDEX(i, j)] -=
                    0.5 * (p.vals[INDEX(i + 1, j)] - p.vals[INDEX(i - 1, j)]) / h;
                v.vals[INDEX(i, j)] -=
                    0.5 * (p.vals[INDEX(i, j + 1)] - p.vals[INDEX(i, j - 1)]) / h;
            }
        }
        set_bnd(1, u);
        set_bnd(2, v);
    }

    void project_d(DoubleArray_D u, DoubleArray_D v, DoubleArray_D p, DoubleArray_D div) {
        int i, j, k;
        double h;
        h = 1.0 / N;
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                div.tgts[INDEX(i, j)] = -0.5 * h *
                    (u.tgts[INDEX(i + 1, j)]
                     - u.tgts[INDEX(i - 1, j)]
                     + v.tgts[INDEX(i, j + 1)]
                     - v.tgts[INDEX(i, j - 1)]) ;
                div.vals[INDEX(i, j)] = -0.5 * h *
                    (u.vals[INDEX(i + 1, j)]
                     - u.vals[INDEX(i - 1, j)]
                     + v.vals[INDEX(i, j + 1)]
                     - v.vals[INDEX(i, j - 1)]) ;
                p.tgts[INDEX(i, j)] = 0;
                p.vals[INDEX(i, j)] = 0;
            }
        }
        set_bnd_d(0, div);
        set_bnd_d(0, p);
        for (k = 0; k < 20; k++) {
            for (i = 1; i <= N; i++) {
                for (j = 1; j <= N; j++) {
                    p.tgts[INDEX(i, j)] =
                        (div.tgts[INDEX(i, j)]
                         + p.tgts[INDEX(i - 1, j)]
                         + p.tgts[INDEX(i + 1, j)]
                         + p.tgts[INDEX(i, j - 1)]
                         + p.tgts[INDEX(i, j + 1)]) / 4;
                    p.vals[INDEX(i, j)] =
                        (div.vals[INDEX(i, j)]
                         + p.vals[INDEX(i - 1, j)]
                         + p.vals[INDEX(i + 1, j)]
                         + p.vals[INDEX(i, j - 1)]
                         + p.vals[INDEX(i, j + 1)]) / 4;
                }
            }
            set_bnd_d(0, p);
        }
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                u.tgts[INDEX(i, j)] -=
                    0.5 * (p.tgts[INDEX(i + 1, j)] - p.tgts[INDEX(i - 1, j)]) / h;
                u.vals[INDEX(i, j)] -=
                    0.5 * (p.vals[INDEX(i + 1, j)] - p.vals[INDEX(i - 1, j)]) / h;
                v.tgts[INDEX(i, j)] -=
                    0.5 * (p.tgts[INDEX(i, j + 1)] - p.tgts[INDEX(i, j - 1)]) / h;
                v.vals[INDEX(i, j)] -=
                    0.5 * (p.vals[INDEX(i, j + 1)] - p.vals[INDEX(i, j - 1)]) / h;
            }
        }
        set_bnd_d(1, u);
        set_bnd_d(2, v);
    }

    void set_bnd(int b, DoubleArray_D x) {
        int i;
        for (i = 1; i <= N; i++) {
            x.vals[INDEX(0, i)] = (b == 1) ? -x.vals[INDEX(1, i)] : x.vals[INDEX(1, i)];
            x.vals[INDEX(N + 1, i)] = b == 1 ? -x.vals[INDEX(N, i)] : x.vals[INDEX(N, i)];
            x.vals[INDEX(i, 0)] = b == 2 ? -x.vals[INDEX(i, 1)] : x.vals[INDEX(i, 1)];
            x.vals[INDEX(i, N + 1)] = b == 2 ? -x.vals[INDEX(i, N)] : x.vals[INDEX(i, N)];
        }
        x.vals[INDEX(0, 0)] = 0.5 * (x.vals[INDEX(1, 0)] + x.vals[INDEX(0, 1)]);
        x.vals[INDEX(0, N + 1)] = 0.5 * (x.vals[INDEX(1, N + 1)] + x.vals[INDEX(0, N)]);
        x.vals[INDEX(N + 1, 0)] = 0.5 * (x.vals[INDEX(N, 0)] + x.vals[INDEX(N + 1, 1)]);
        x.vals[INDEX(N + 1, N + 1)] = 0.5 * (x.vals[INDEX(N, N + 1)] + x.vals[INDEX(N + 1, N)]);
    }

    void set_bnd_d(int b, DoubleArray_D x) {
        int i;
        for (i = 1; i <= N; i++) {
            x.tgts[INDEX(0, i)] = (b == 1) ? -x.tgts[INDEX(1, i)] : x.tgts[INDEX(1, i)];
            x.vals[INDEX(0, i)] = (b == 1) ? -x.vals[INDEX(1, i)] : x.vals[INDEX(1, i)];
            x.tgts[INDEX(N + 1, i)] = b == 1 ? -x.tgts[INDEX(N, i)] : x.tgts[INDEX(N, i)];
            x.vals[INDEX(N + 1, i)] = b == 1 ? -x.vals[INDEX(N, i)] : x.vals[INDEX(N, i)];
            x.tgts[INDEX(i, 0)] = b == 2 ? -x.tgts[INDEX(i, 1)] : x.tgts[INDEX(i, 1)];
            x.vals[INDEX(i, 0)] = b == 2 ? -x.vals[INDEX(i, 1)] : x.vals[INDEX(i, 1)];
            x.tgts[INDEX(i, N + 1)] = b == 2 ? -x.tgts[INDEX(i, N)] : x.tgts[INDEX(i, N)];
            x.vals[INDEX(i, N + 1)] = b == 2 ? -x.vals[INDEX(i, N)] : x.vals[INDEX(i, N)];
        }
        x.tgts[INDEX(0, 0)] = 0.5 * (x.tgts[INDEX(1, 0)] + x.tgts[INDEX(0, 1)]);
        x.vals[INDEX(0, 0)] = 0.5 * (x.vals[INDEX(1, 0)] + x.vals[INDEX(0, 1)]);
        x.tgts[INDEX(0, N + 1)] = 0.5 * (x.tgts[INDEX(1, N + 1)] + x.tgts[INDEX(0, N)]);
        x.vals[INDEX(0, N + 1)] = 0.5 * (x.vals[INDEX(1, N + 1)] + x.vals[INDEX(0, N)]);
        x.tgts[INDEX(N + 1, 0)] = 0.5 * (x.tgts[INDEX(N, 0)] + x.tgts[INDEX(N + 1, 1)]);
        x.vals[INDEX(N + 1, 0)] = 0.5 * (x.vals[INDEX(N, 0)] + x.vals[INDEX(N + 1, 1)]);
        x.tgts[INDEX(N + 1, N + 1)] = 0.5 * (x.tgts[INDEX(N, N + 1)] + x.tgts[INDEX(N + 1, N)]);
        x.vals[INDEX(N + 1, N + 1)] = 0.5 * (x.vals[INDEX(N, N + 1)] + x.vals[INDEX(N + 1, N)]);
    }

    final int INDEX(int i, int j) {
        return i + (N + 2) * j;
    }

}
