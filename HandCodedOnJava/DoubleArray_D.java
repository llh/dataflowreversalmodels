/** A pair of an double array variable and their tangent derivatives.
 * This is equivalent to an array of Double_D,
 * but avoids inflation of the number of Objects */
public class DoubleArray_D {

    public double[] vals ;
    public double[] tgts ;

    public DoubleArray_D(int size) {
        vals = new double[size] ;
        tgts = new double[size] ;
    }
}
