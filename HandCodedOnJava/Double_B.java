
import java.io.*;

/** A pair of a primal variable and its adjoint derivative, in Association-by-Address fashion.
 * Same as the classical aDouble, except for the ADRestorable interface */
public class Double_B implements ADRestorable, AutoCloseable {
    public double val ;
    public double adj = 0.0 ;
    private int objectKey = -1 ;

    public Double_B() {
        this.objectKey = ADStack.registerObject(this) ;
        this.val = 0.0 ;
        this.adj = 0.0 ;
    }

    public Double_B(double v, double d) {
        this.objectKey = ADStack.registerObject(this) ;
        this.val = v ;
        this.adj = d ;
    }

    public void new_bwd(Todouble valb) {
        valb.set(adj) ;
        ADStack.unRegisterObject(this) ;
    }

    public void setObjectKey(int objectKey) {
        this.objectKey = objectKey ;
    }

    public int objectKey() {
        return objectKey ;
    }

    public void finalize() {
// System.out.println("  FINALIZING "+this) ;
        if (objectKey!=-1) ADStack.unRegisterOneObject(objectKey, this) ;
    }

    public void close() {
// System.out.println("     CLOSING "+this) ;
        if (objectKey!=-1) ADStack.unRegisterOneObject(objectKey, this) ;
    }

    public void storeFields() {
        ADStack.plainPushDouble(val) ;
    }

    public void restoreFields() {
        val = ADStack.plainPopDouble() ;
    }

    public void restoreObjectField(int fieldIndex, Object destination) {
        // No Object field in a Double_B!
    }

    public String toString() {return "[Double_B:"+objectKey+"]("+val+":"+adj+")" ;}

}
