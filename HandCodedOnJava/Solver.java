
public class Solver {
    int N ;
    int SIZE ;
    double[] u ;
    double[] v ;
    double[] u_prev ;
    double[] v_prev ;
    double[] dense ;
    double[] dense_prev ;
    ChainedList timeControls ;
    double[] argx ;
    double[] argx0 ;

    public Solver(int N) {
        this.N = N ;
        SIZE = (N + 2) * (N + 2);
        u = new double[SIZE];
        v = new double[SIZE];
        u_prev = new double[SIZE];
        v_prev = new double[SIZE];
        dense = new double[SIZE];
        dense_prev = new double[SIZE];
    }

    public void applyControl(double du[], double dv[]) {
        int ii2 = 82, jj2 = 30 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = N/2 ;
        }
        for (int i=0 ; i<ii2 ; ++i) {
            int index = INDEX(jj2,i) ;
            u[index] += du[i] ;
            v[index] += dv[i] ;
        }
    }

    public double computeCost() {
        int ii2 = 82, jj2 = 70 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = (3*N)/4 ;
        }
        double cost = 0.0 ;
        for (int i=0 ; i<ii2 ; ++i) {
            int index = INDEX(i,jj2) ;
            cost += u[index]*u[index] + v[index]*v[index] ;
        }
        return cost ;
    }

    public void ticks(int nbTicks, double dt, double visc, double diff, double ct) {
        timeControls = null ;
        for (int i=1 ; i<=nbTicks ; ++i) {
            timeControls = new ChainedList(new Double(ct), timeControls) ;
        }
        for (int i=1 ; i<=nbTicks ; ++i) {
            tick(dt, visc, diff) ;
        }
    }

    void tick(double dt, double visc, double diff) {
        vel_step(u, v, u_prev, v_prev, visc, dt);
        dens_step(dense, dense_prev, u, v, diff, dt);
    }

    void vel_step(double[] u, double[] v, double[] u0, double[] v0, double visc,
                  double dt) {
        int ii2 = 40 ;
        if (N<80) {
            ii2 = N/2 ;
        }
        u[INDEX(ii2,ii2)] += ((Double)timeControls.head).doubleValue() ;
        timeControls = timeControls.tail ;
        add_source(u, u0, dt);
        add_source(v, v0, dt);

        {
//             diffuse(1, argu1, argu2, visc, dt);
//             diffuse(2, argv1, argv2, visc, dt);
            argx = u0 ;
            argx0 = u ;
            diffuse(1, visc, dt);
            argx = v0 ;
            argx0 = v ;
            diffuse(2, visc, dt);
            double[] argu1 = u0 ;
            double[] argu2 = u ;
            double[] argv1 = v0 ;
            double[] argv2 = v ;
            project(argu1, argv1, argu2, argv2);
        }

        advect(1, u, u0, u0, v0, dt);
        advect(2, v, v0, u0, v0, dt);
        project(u, v, u0, v0);

    }

    void dens_step(double[] x, double[] x0, double[] u, double[] v, double diff,
            double dt) {
        argx = x0 ;
        argx0 = x ;
        diffuse(0, diff, dt);
        argx = x ;
        argx0 = x0 ;
        diffuse(0, diff, dt);
    }

    void add_source(double[] x, double[] s, double dt) {
        int i, size = (N + 2) * (N + 2);
        for (i = 0; i < size; i++)
            x[i] += dt * s[i];
    }

    void diffuse(int b, double diff, double dt) {
        int i, j, k;
        double a = dt * diff * N * N;
        for (k = 0; k < 20; k++) {
            for (i = 1; i <= N; i++) {
                for (j = 1; j <= N; j++) {
                    argx[INDEX(i, j)] = (argx0[INDEX(i, j)] + a
                            * (argx[INDEX(i - 1, j)] + argx[INDEX(i + 1, j)] + argx[INDEX(i, j - 1)] + argx[INDEX(
                                    i, j + 1)]))
                            / (1 + 4 * a);
                }
            }
            set_bnd(b, argx);
        }
    }

    void advect(int b, double[] d, double[] d0, double[] u, double[] v, double dt) {
        int i, j, i0, j0, i1, j1;
        double x, y, s0, t0, s1, t1, dt0;
        dt0 = dt * N;
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                x = i - dt0 * u[INDEX(i, j)];
                y = j - dt0 * v[INDEX(i, j)];
                if (x < 0.5)
                    x = 0.5;
                if (x > N + 0.5)
                    x = N + 0.5;
                i0 = (int) x;
                i1 = i0 + 1;
                if (y < 0.5)
                    y = 0.5;
                if (y > N + 0.5)
                    y = N + 0.5;
                j0 = (int) y;
                j1 = j0 + 1;
                s1 = x - i0;
                s0 = 1 - s1;
                t1 = y - j0;
                t0 = 1 - t1;
                d[INDEX(i, j)] = s0 * (t0 * d0[INDEX(i0, j0)] + t1 * d0[INDEX(i0, j1)])
                        + s1 * (t0 * d0[INDEX(i1, j0)] + t1 * d0[INDEX(i1, j1)]);
            }
        }
        set_bnd(b, d);
    }

    void project(double[] u, double[] v, double[] p, double[] div) {
        int i, j, k;
        double h;
        h = 1.0 / N;
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                div[INDEX(i, j)] = -0.5
                        * h
                        * (u[INDEX(i + 1, j)] - u[INDEX(i - 1, j)] + v[INDEX(i, j + 1)] - v[INDEX(
                                i, j - 1)]);
                p[INDEX(i, j)] = 0;
            }
        }
        set_bnd(0, div);
        set_bnd(0, p);
        for (k = 0; k < 20; k++) {
            for (i = 1; i <= N; i++) {
                for (j = 1; j <= N; j++) {
                    p[INDEX(i, j)] = (div[INDEX(i, j)] + p[INDEX(i - 1, j)]
                            + p[INDEX(i + 1, j)] + p[INDEX(i, j - 1)] + p[INDEX(i, j + 1)]) / 4;
                }
            }
            set_bnd(0, p);
        }
        for (i = 1; i <= N; i++) {
            for (j = 1; j <= N; j++) {
                u[INDEX(i, j)] -= 0.5 * (p[INDEX(i + 1, j)] - p[INDEX(i - 1, j)]) / h;
                v[INDEX(i, j)] -= 0.5 * (p[INDEX(i, j + 1)] - p[INDEX(i, j - 1)]) / h;
            }
        }
        set_bnd(1, u);
        set_bnd(2, v);
    }

    void set_bnd(int b, double[] x) {
        int i;
        for (i = 1; i <= N; i++) {
            x[INDEX(0, i)] = (b == 1) ? -x[INDEX(1, i)] : x[INDEX(1, i)];
            x[INDEX(N + 1, i)] = b == 1 ? -x[INDEX(N, i)] : x[INDEX(N, i)];
            x[INDEX(i, 0)] = b == 2 ? -x[INDEX(i, 1)] : x[INDEX(i, 1)];
            x[INDEX(i, N + 1)] = b == 2 ? -x[INDEX(i, N)] : x[INDEX(i, N)];
        }
        x[INDEX(0, 0)] = 0.5 * (x[INDEX(1, 0)] + x[INDEX(0, 1)]);
        x[INDEX(0, N + 1)] = 0.5 * (x[INDEX(1, N + 1)] + x[INDEX(0, N)]);
        x[INDEX(N + 1, 0)] = 0.5 * (x[INDEX(N, 0)] + x[INDEX(N + 1, 1)]);
        x[INDEX(N + 1, N + 1)] = 0.5 * (x[INDEX(N, N + 1)] + x[INDEX(N + 1, N)]);
    }

    final int INDEX(int i, int j) {
        return i + (N + 2) * j;
    }

}
