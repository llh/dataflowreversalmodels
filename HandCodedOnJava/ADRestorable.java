public interface ADRestorable {

    public void setObjectKey(int key) ;

    public int objectKey() ;

    /** Stores onto the stack the contents of this restorable Object. */
    public void storeFields() ;

    /** Restore the fields of this restorable Object, after it has just been re-registered
     * i.e. it has been re-built for the backward sweep.
     * The particular restorable Object should know what data is on the stack,
     * how to retrieve it, and where to place it back in this Object's fields */
    public void restoreFields() ;

    /** Delayed restoration of an "Object" field of this restorable Object.
     * Sometimes restoreFields() cannot find the true contents of one Object field,
     * because the corresponding destination Object has not been rebuilt yet.
     * Later, when the destination Object is rebuilt, this method is called to
     * finally attach the rebuilt destination in the correct "Object" field. */
    public void restoreObjectField(int fieldIndex, Object destination) ;
}
