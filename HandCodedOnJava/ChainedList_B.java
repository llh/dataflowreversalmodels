import java.io.*;

public class ChainedList_B implements ADRestorable, AutoCloseable {
    public Object head ;
    public ChainedList_B tail ;
    private int objectKey = -1 ;

    public ChainedList_B() {
        this.objectKey = ADStack.registerObject(this) ;
        head = null ;
        tail = null ;
    }

    public ChainedList_B(Object head, ChainedList_B tail) {
        this.objectKey = ADStack.registerObject(this) ;
        this.head = head ;
        this.tail = tail ;
    }

    public void new_bwd(Object hd, ChainedList_B tl) {
        ADStack.unRegisterObject(this) ;
    }

    public void setObjectKey(int objectKey) {
        this.objectKey = objectKey ;
    }

    public int objectKey() {
        return objectKey ;
    }

    public void finalize() {
// System.out.println("  FINALIZING "+this) ;
        if (objectKey!=-1) ADStack.unRegisterOneObject(objectKey, this) ;
    }

    public void close() {
// System.out.println("     CLOSING "+this) ;
        if (objectKey!=-1) ADStack.unRegisterOneObject(objectKey, this) ;
    }

    public void storeFields() {
        ADStack.plainPushInteger((head==null ? -1 : ((ADRestorable)head).objectKey())) ;
        ADStack.plainPushInteger((tail==null ? -1 : tail.objectKey)) ;
    }

    public void restoreFields() {
        tail = (ChainedList_B)ADStack.findReCreatedObjectOrWait(ADStack.plainPopInteger(), this, 2) ;
        head = ADStack.findReCreatedObjectOrWait(ADStack.plainPopInteger(), this, 1) ;
    }

    public void restoreObjectField(int fieldIndex, Object destination) {
        if (fieldIndex==1)
            head = destination ;
        else if (fieldIndex==2)
            tail = (ChainedList_B)destination ;
    }

    public String toString() {return "[ChainedList_B:"+objectKey+"]"+toStringRec(null) ;}

    private String toStringRec(ChainedList dejaVu) {
        int cycleRank = (dejaVu==null?-1:dejaVu.rank(this)) ;
        if (cycleRank!=-1) {
            return "CYCLE-"+(cycleRank+1) ;
        } else {
            ChainedList_B listTail = this;
            String result = null ;
            cycleRank = -1 ;
            while (listTail != null && (cycleRank = dejaVu == null ? -1 : dejaVu.rank(listTail)) == -1) {
                result = (result==null?"(":result+" ") ;
                dejaVu = new ChainedList(listTail, dejaVu) ;
                if (listTail.head instanceof ChainedList_B) {
                    result = result + ((ChainedList_B)listTail.head).toStringRec(dejaVu) ;
                } else {
                    result = result + listTail.head;
                }
                listTail = listTail.tail;
            }
            if (listTail!=null)
                result = result+" . "+(cycleRank==-1?listTail:"CYCLE-"+(cycleRank+1));
            result = result + ")";
            return result;
        }
    }

    public int rank(Object obj) {
        int rank = 0;
        ChainedList_B inList = this;
        while (inList!=null && inList.head!=obj) {
            ++rank ;
            inList = inList.tail ;
        }
        return (inList==null?-1:rank) ;
    }

}
