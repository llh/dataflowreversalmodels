import java.io.*;
import java.lang.ref.* ;

/** Stack mechanism for communication from the forward sweep to the backward sweep
 * of ajoint code (aka ``data-flow reversal'').
 * Provides primitives for restoration of Objects despite of GC */
public final class ADStack {

    /** Internal storage of correspondence from Object's key to Object */ 
    private static ADStackTable objectsTable = null ;

    private static Object synchronizationObject = new Object() ;

    private static int topKey = -1 ;

    /** Register an Object newly created, during a forward sweep.
     * Registration keeps a "Soft/Weak" reference to the Object, so that this doesn't
     * prevent the GC to free the Object when no one refers to it.*/
    public static int registerObject(Object objb) {
        // Don't register when restoring Objects in the backward sweep:
        if (poppingGCed) return -1 ;
        ++topKey ;
        //Note: we may try with WeakReference or SoftReference...
        objectsTable = new ADStackTable(topKey, new WeakReference<Object>(objb), objectsTable) ;
        //((ADRestorable)objb).setObjectKey(topKey) ;//redundant, done in new objb()
// System.out.println("REGISTERED "+objb+" INTO TABLE, KEY "+topKey) ;
        return topKey ;
    }

    /** Un-registers the Object registered through registerObject().
     * This is called in the backward sweep, at the mirroring location of the registerObject().
     * This erases the Object's entry in objectsTable, allowing the Object to be GC'ed */
    public static void unRegisterObject(Object objb) {
// System.out.println("FINAL UNREGISTERING OF OBJECT: "+objb) ;
        // New code: objectsTable can be cleaned in any order, possibly leaving holes in the numbering:
        ADStackTable toMainTable = new ADStackTable(-1, null, objectsTable) ;
        ADStackTable inMainTable = toMainTable ;
        int searched = ((ADRestorable)objb).objectKey() ;
        boolean found = false ;
        while (inMainTable!=null && inMainTable.next!=null && !found) {
            if (inMainTable.next.key > searched)
                inMainTable = inMainTable.next ;
            else if (inMainTable.next.key < searched)
                inMainTable = null ;
            else
                found = true ;
        }
        if (found)
            inMainTable.next = inMainTable.next.next ;
        else
            System.out.println("Error: key "+searched+" not found in objectsTable") ;
        objectsTable = toMainTable.next ;
    }

    /** Unregister the table entry for one Object, at the moment when this Object is GC'ed, in general
     * during the forward sweep, but this can also occur later, when already in the backward sweep.
     * Stores the unregistered object onto the ADStack. */
    public static void unRegisterOneObject(int keyInTable, Object vanishingObject) {
System.out.print("+");
// System.out.println("PUSHING ONTO STACK VANISHING OBJECT: "+vanishingObject) ;
        synchronized (synchronizationObject) {
            ADStackTable tableEntry = getRemoveTableEntry(keyInTable) ;
            ((ADRestorable)vanishingObject).storeFields() ;
            plainPushClass(vanishingObject.getClass()) ;
            plainPushInteger(keyInTable) ;
            plainPushBoolean(true) ; // next thing to pop IS a GC'ed object.
            tableEntry.toObject = null ;
        }
    }

    /** Re-creates an Object and a table Entry for it, using data stored on the ADStack.
     * Assumes that the current top of the ADStack contains the storage of an Object
     * (i.e. a top integer table key, then the Class, then the Object contents).
     * rebuilds a new Object, associates it in the objectsTable with the given table key,
     * fills it with the contents, and manages the waiting list of Objects not yet re-created. */
    private static void reRegisterOneObject() {
        int keyInTable = plainPopInteger() ;
        Object rebuiltObject = null ;
        try {
// System.out.print("-"); 
// System.out.print("POPPING TOP GC'ed CLASS FROM:");
// for (int i=classIndex ; i>=0&&(classIndex-i<20) ; --i) System.out.print(" "+classStack[i].getCanonicalName()) ;
// System.out.println() ;
            rebuiltObject = plainPopClass().newInstance() ;
        } catch (Exception exception) {
            System.out.println("(fatal) Could not rebuild a newInstance, exception:"+exception) ;
        }
        ((ADRestorable)rebuiltObject).restoreFields() ;
        ((ADRestorable)rebuiltObject).setObjectKey(keyInTable) ;
        ADStackTable tableEntry = getSetTableEntry(keyInTable) ;
        if (tableEntry.toObject!=null)
            System.out.println("Error: reRegisterOneObject: Object at key "+keyInTable
                               +" is already rebuilt:"+tableEntry.toObject) ;
        tableEntry.toObject = rebuiltObject ;
// System.out.println("REBUILT VANISHED OBJECT FROM STACK: "+rebuiltObject) ;
        WaitingRefsList waitingRefs = tableEntry.waitingRefs ;
        while (waitingRefs!=null) {
            ((ADRestorable)waitingRefs.referent).restoreObjectField(waitingRefs.field, rebuiltObject) ;
            waitingRefs = waitingRefs.next ;
        }
        tableEntry.waitingRefs = null ;
    }

    /** Finds in the objectsTable the Object re-created for the given keyInTable.
     * It is the task of the calling method to attach the returned re-created Object
     * at the correct location "referentFieldIndex" in the correct "referent" Object.
     * If there is no re-created Object yet, returns null and registers this fact,
     * so that when the Object for keyInTable is finally created, it will immediately
     * be attached to location "referentFieldIndex" in "referent". */
    public static Object findReCreatedObjectOrWait(int keyInTable, Object referent, int referentFieldIndex) {
        if (keyInTable==-1) return null ;
        ADStackTable tableEntry = getSetTableEntry(keyInTable) ;
        if (tableEntry.toObject != null)
            return tableEntry.toObject ;
        else {
            tableEntry.waitingRefs = new WaitingRefsList(referent, referentFieldIndex, tableEntry.waitingRefs) ;
            return null ;
        }
    }

    /** Returns the Object corresponding to the recently popped "keyInTable".
     * Assumes the Object at "keyInTable" has already been re-created (or possibly
     * recycled with its forward sweep value when it was not GC'ed) and registered
     * at "keyInTable" in the objectsTable. */
    public static Object restoreObjectFromKey(int keyInTable) {
        if (keyInTable==-1) return null ;
        ADStackTable tableEntry = getTableEntry(keyInTable) ;
        if (tableEntry==null)
            System.out.println("Error: restoreObjectFromKey could not find entry "+keyInTable+" in objectsTable!") ;
        if (tableEntry.toObject instanceof Reference) {
            // If object from the fwd sweep has not been GC'ed yet,
            // recycle it for the bwd sweep before it is too late:
            // now reference it again, so it will not be GC'ed any more.
            tableEntry.toObject = ((Reference)tableEntry.toObject).get() ;
// System.out.println("RECYCLING OBJECT BEFORE GC SEES IT: "+tableEntry.toObject) ;
        }
        return tableEntry.toObject ;
    }

    public static void pushObjRef(Object objb) {
        synchronized (synchronizationObject) {
            plainPushInteger(objb==null ? -1 : ((ADRestorable)objb).objectKey()) ;
            plainPushBoolean(false) ; // next thing to pop is not a GC'ed object
        }
    }

    public static Object popObjRef() {
        Object result = null ;
        synchronized (synchronizationObject) {
            popGCed() ; // while next thing to pop is a GC'ed object, pop it and re-create it
            result = restoreObjectFromKey(plainPopInteger()) ;
        }
        return result ;
    }

    public static void pushBoolean(boolean value) {
        synchronized (synchronizationObject) {
            plainPushBoolean(value) ;
            plainPushBoolean(false) ; // next thing to pop is not a GC'ed object
        }
    }

    public static boolean popBoolean() {
        boolean result = false ;
        synchronized (synchronizationObject) {
            popGCed() ; // while next thing to pop is a GC'ed object, pop it and re-create it
            result = plainPopBoolean() ;
        }
        return result ;
    }

    public static void pushControl(int value) {
        synchronized (synchronizationObject) {
            plainPushControl(value) ;
            plainPushBoolean(false) ; // next thing to pop is not a GC'ed object
        }
    }

    public static int popControl() {
        int result = -1 ;
        synchronized (synchronizationObject) {
            popGCed() ; // while next thing to pop is a GC'ed object, pop it and re-create it
            result = plainPopControl() ;
        }
        return result ;
    }

    public static void pushInteger(int value) {
        synchronized (synchronizationObject) {
            plainPushInteger(value) ;
            plainPushBoolean(false) ; // next thing to pop is not a GC'ed object
        }
    }

    public static int popInteger() {
        int result = -1 ;
        synchronized (synchronizationObject) {
            popGCed() ; // while next thing to pop is a GC'ed object, pop it and re-create it
            result = plainPopInteger() ;
        }
        return result ;
    }

    public static void pushDouble(double value) {
        synchronized (synchronizationObject) {
            plainPushDouble(value) ;
            plainPushBoolean(false) ; // next thing to pop is not a GC'ed object
        }
    }

    public static double popDouble() {
        double result = 0.0 ;
        synchronized (synchronizationObject) {
            popGCed() ; // while next thing to pop is a GC'ed object, pop it and re-create it
            result = plainPopDouble() ;
        }
        return result ;
    }

    /** true when we are actually popping GC'ed object, false otherwise.
     * When poppingGCed==true, new Objects created must not receive a new
     * registration key: they must receive the same key as in the forward sweep */
    private static boolean poppingGCed = false ;

    /** As long as the top of stack is a GC'ed object, pop it, and re-create the corresponding
     * entry in objectsTable. When re-creating, create a new Object of this entry's saved Class,
     * fill it with whatever field values have been saved. For fields of Object type,
     * either this field's stored objectKey is already available in objectsTable,
     * in which case use the associated Object,
     * or else get/set the entry for this objectKey in the objectsTable, without any Object, and
     * put the current new Object into the waiting list.
     * To be run before each popPointer, and at the bwd correspondent
     * of a closing scope "}" (to be refined for tbr) */
    private static void popGCed() {
        poppingGCed = true ;
        boolean mustPopGC ;
        // while next thing to pop is a GC'ed object, pop it and re-create it:
        while (mustPopGC = plainPopBoolean()) {
            reRegisterOneObject() ;
        }
        poppingGCed = false ;
    }

    private final static class ADStackTable {
        public int key ;
        public Object toObject ;
        public WaitingRefsList waitingRefs = null ;
        public ADStackTable next ;
        public ADStackTable(int key, Reference toObject, ADStackTable next) {
            this.key = key ;
            this.toObject = toObject ;
            this.next = next ;
        }
    }

    private static ADStackTable getTableEntry(int keyInTable) {
        ADStackTable inMainTable = objectsTable ;
        while (inMainTable!=null && inMainTable.key>keyInTable) {
            inMainTable = inMainTable.next ;
        }
        return ((inMainTable==null || inMainTable.key<keyInTable) ? null : inMainTable) ;
    }

    private static ADStackTable getSetTableEntry(int keyInTable) {
        ADStackTable toMainTable = new ADStackTable(-1, null, objectsTable) ;
        ADStackTable inMainTable = toMainTable ;
        while (inMainTable.next!=null && inMainTable.next.key>keyInTable) {
            inMainTable = inMainTable.next ;
        }
        if (inMainTable.next==null || inMainTable.next.key!=keyInTable) {
            inMainTable.next = new ADStackTable(keyInTable, null, inMainTable.next) ;
            objectsTable = toMainTable.next ;
        }
        return inMainTable.next ;
    }

    private static ADStackTable getRemoveTableEntry(int keyInTable) {
        ADStackTable toMainTable = new ADStackTable(-1, null, objectsTable) ;
        ADStackTable inMainTable = toMainTable ;
        while (inMainTable.next!=null && inMainTable.next.key>keyInTable) {
            inMainTable = inMainTable.next ;
        }
        if (inMainTable.next==null || inMainTable.next.key<keyInTable)
            System.out.println("Error: getRemoveTableEntry could not find entry "+keyInTable+" in objectsTable!") ;

        ADStackTable tableEntry = inMainTable.next ;
        inMainTable.next = inMainTable.next.next ;
        tableEntry.next = null ;
        objectsTable = toMainTable.next ;
        return tableEntry ;
    }

    private final static class WaitingRefsList {
        public Object referent ;
        public int field ;
        public WaitingRefsList next ;
        public WaitingRefsList(Object referent, int field, WaitingRefsList next) {
            this.referent = referent ;
            this.field = field ;
            this.next = next ;
        }
    }

// JNI LINK TO THE STANDARD adStack.c:
//====================================

    /** jni link to Tapenade adStack.c */
    static {
        System.loadLibrary("adStack") ;
    }
    public static native void plainPushBoolean(boolean value) ;
    public static native boolean plainPopBoolean() ;
    public static native void plainPushInteger(int value) ;
    public static native int plainPopInteger() ;
    public static native void plainPushDouble(double value) ;
    public static native double plainPopDouble() ;


    public static void plainPushClass(Class<?> value) {
        int classCode = -1 ;
        if (value==Double_B.class)
            classCode = 1 ;
        else if (value==DoubleArray_B.class)
            classCode = 1 ;
        else if (value==ChainedList_B.class)
            classCode = 3 ;
        plainPushInteger(classCode) ;
    }

    public static Class<?> plainPopClass() {
        int classCode = plainPopInteger() ;
        switch (classCode) {
        case 1:
            return Double_B.class ;
        case 2:
            return DoubleArray_B.class ;
        case 3:
            return ChainedList_B.class ;
        default:
            return null;
        }
    }


    public static void plainPushControl(int value) {plainPushInteger(value) ;}

    public static int plainPopControl() {return plainPopInteger() ;}

    public static void show(String label) {
        System.out.println("STACKS AT "+label+", now stored in standard adStack") ;
    }

    public static native void showPeakSize() ;


// // Actual storage in local stack
// // =============================

//     private static final int sizefactor = 2000 ; // if over 3000, requires java -Xms -Xmx

//     // Classes:
//     private static Class<?>[] classStack = new Class<?>[sizefactor*1] ;

//     private static int classIndex = -1 ;

//     public static void plainPushClass(Class<?> value) {classStack[++classIndex] = value ;}

//     public static Class<?> plainPopClass() {return classStack[classIndex--] ;}

//     // Booleans:
//     private static boolean[] boolStack = new boolean[sizefactor*250000] ;

//     private static int boolIndex = -1 ;

//     public static void plainPushBoolean(boolean value) {boolStack[++boolIndex] = value ;}

//     public static boolean plainPopBoolean() {return boolStack[boolIndex--] ;}

//     // Control:
//     private static int[] controlStack = new int[sizefactor*25000] ;

//     private static int controlIndex = -1 ;

//     public static void plainPushControl(int value) {controlStack[++controlIndex] = value ;}

//     public static int plainPopControl() {return controlStack[controlIndex--] ;}

//     //Integers:
//     private static int[] integerStack = new int[sizefactor*23000] ;

//     private static int integerIndex = -1 ;

//     public static void plainPushInteger(int value) {integerStack[++integerIndex] = value ;}

//     public static int plainPopInteger() {return integerStack[integerIndex--] ;}

//     //Doubles:
//     private static double[] doubleStack = new double[sizefactor*250000] ;

//     private static int doubleIndex = -1 ;

//     public static void plainPushDouble(double value) {doubleStack[++doubleIndex] = value ;}

//     public static double plainPopDouble() {return doubleStack[doubleIndex--] ;}

//     public static void show(String label) {
//         System.out.println() ;
//         System.out.println("STACKS AT "+label) ;
//         System.out.println("------------------") ;
//         System.out.print("   classes ("+(classIndex+1)+")  :") ;
//         for (int i=classIndex ; i>=0&&(classIndex-i<20) ; --i) System.out.print(" "+classStack[i].getCanonicalName()) ;
//         System.out.println() ;
//         System.out.print("   booleans ("+(boolIndex+1)+")  :") ;
//         for (int i=boolIndex ; i>=0&&(boolIndex-i<100) ; --i) System.out.print((boolStack[i]?" t":" f")) ;
//         System.out.println() ;
//         System.out.print("   controls ("+(controlIndex+1)+")  :") ;
//         for (int i=controlIndex ; i>=0&&(controlIndex-i<20) ; --i) System.out.print(" "+controlStack[i]) ;
//         System.out.println() ;
//         System.out.print("   integers ("+(integerIndex+1)+")  :") ;
//         for (int i=integerIndex ; i>=0&&(integerIndex-i<20) ; --i) System.out.print(" "+integerStack[i]) ;
//         System.out.println() ;
//         System.out.print("   doubles ("+(doubleIndex+1)+")  :") ;
//         for (int i=doubleIndex ; i>=0&&(doubleIndex-i<20) ; --i) System.out.print(" "+doubleStack[i]) ;
//         System.out.println() ;
//         System.out.println("TABLE AT "+label+(objectsTable==null?" : EMPTY":"")) ;
//         ADStackTable inMainTable = objectsTable ;
//         while (inMainTable!=null) {
//             System.out.println("   "+inMainTable.key+" : "
//                                +(inMainTable.toObject instanceof Reference?
//                                  "WEAK : "+((Reference)inMainTable.toObject).get():
//                                  inMainTable.toObject)
//                                +" WAITING:"+inMainTable.waitingRefs) ;
//             inMainTable = inMainTable.next ;
//         }
//     }

//     public static void showPeakSize() {
//         // will be implemented when using jni!
//     }

}
