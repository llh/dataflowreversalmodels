
/** Adjoint AD of Class Solver. */
public class Solver_B {
    int N ;
    int SIZE ;
    DoubleArray_B u ;
    DoubleArray_B v ;
    DoubleArray_B u_prev ;
    DoubleArray_B v_prev ;
    DoubleArray_B dense ;
    DoubleArray_B dense_prev ;
    ChainedList_B timeControls ;
    DoubleArray_B argx ;
    DoubleArray_B argx0 ;

    public Solver_B(int N) {
        this.N = N;
        SIZE = (N + 2) * (N + 2);
        u = new DoubleArray_B(SIZE) ;
        v = new DoubleArray_B(SIZE) ;
        u_prev = new DoubleArray_B(SIZE) ;
        v_prev = new DoubleArray_B(SIZE) ;
        dense = new DoubleArray_B(SIZE) ;
        dense_prev = new DoubleArray_B(SIZE) ;
    }

    public void new_bwd() {
        ADStack.unRegisterObject(u) ;
        ADStack.unRegisterObject(v) ;
        ADStack.unRegisterObject(u_prev) ;
        ADStack.unRegisterObject(v_prev) ;
        ADStack.unRegisterObject(dense) ;
        ADStack.unRegisterObject(dense_prev) ;
    }

    public void applyControl_fwd(DoubleArray_B du, DoubleArray_B dv) {
        int ii2 = 82, jj2 = 30 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = N/2 ;
        }
        for (int i=0 ; i<ii2 ; ++i) {
            int index = INDEX(jj2,i) ;
            u.vals[index] += du.vals[i] ;
            v.vals[index] += dv.vals[i] ;
        }
    }

    public void applyControl_bwd(DoubleArray_B du, DoubleArray_B dv) {
        int ii2 = 82, jj2 = 30 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = N/2 ;
        }
        for (int i=ii2-1 ; i>=0 ; --i) {
            int index = INDEX(jj2,i) ;
            dv.adjs[i] += v.adjs[index] ;
            du.adjs[i] += u.adjs[index] ;
        }
    }

    public void computeCost_b(double costb) {
        int ii2 = 82, jj2 = 70 ;
        if (N<80) {
            ii2 = N+2 ;
            jj2 = (3*N)/4 ;
        }
        for (int i=ii2-1 ; i>=0 ; --i) {
            int index = INDEX(i,jj2) ;
            u.adjs[index] += 2.0*u.vals[index] * costb ;
            v.adjs[index] += 2.0*v.vals[index] * costb ;
        }
        costb = 0.0 ;
    }

    public void ticks_fwd(int nbTicks, double dt, double visc, double diff, double ct) {
        timeControls = null ;
        for (int i=1 ; i<=nbTicks ; ++i) {
            Double_B tmp = new Double_B(ct, 0.0) ;
            ADStack.pushObjRef(timeControls) ;
            timeControls = new ChainedList_B(tmp, timeControls) ;
            ADStack.pushObjRef(tmp) ;
        }
        for (int i=1 ; i<=nbTicks ; ++i) {
            tick_fwd(dt, visc, diff) ;
// System.out.println("********************************************* FWD TICK "+i) ;
        }
    }

    public void ticks_bwd(int nbTicks, double dt, double visc, double diff, Todouble ctb) {
        for (int i=nbTicks ; i>=1 ; --i) {
// System.out.println("********************************************* BWD TICK "+i) ;
            tick_bwd(dt, visc, diff) ;
        }
        for (int i=nbTicks ; i>=1 ; --i) {
            Double_B tmp = (Double_B)ADStack.popObjRef() ;
            ChainedList_B timeControls_new = timeControls ;
            timeControls = (ChainedList_B)ADStack.popObjRef() ;
            timeControls_new.new_bwd(tmp, timeControls) ;
            Todouble tmp2 = new Todouble() ;
            tmp.new_bwd(tmp2) ;
            ctb.set(ctb.get()+tmp2.get()) ;
        }
    }

    void tick_fwd(double dt, double visc, double diff) {
        vel_step_fwd(u, v, u_prev, v_prev, visc, dt);
        dens_step_fwd(dense, dense_prev, u, v, diff, dt);
    }

    void tick_bwd(double dt, double visc, double diff) {
        dens_step_bwd(dense, dense_prev, u, v, diff, dt);
        vel_step_bwd(u, v, u_prev, v_prev, visc, dt);
    }

    void vel_step_fwd(DoubleArray_B u, DoubleArray_B v, DoubleArray_B u0, DoubleArray_B v0, double visc,
                  double dt) {
        int ii2 = 40 ;
        if (N<80) {
            ii2 = N/2 ;
        }
        ADStack.pushDouble(u.vals[INDEX(ii2,ii2)]) ;
        u.vals[INDEX(ii2,ii2)] += ((Double_B)timeControls.head).val ;
        ADStack.pushObjRef(timeControls) ;
        timeControls = timeControls.tail ;
        add_source_fwd(u, u0, dt);
        add_source_fwd(v, v0, dt);
        {
            ADStack.pushObjRef(argx) ;
            argx = u0 ;
            ADStack.pushObjRef(argx0) ;
            argx0 = u ;
            diffuse_fwd(1, visc, dt);
            ADStack.pushObjRef(argx) ;
            argx = v0 ;
            ADStack.pushObjRef(argx0) ;
            argx0 = v ;
            diffuse_fwd(2, visc, dt);
            DoubleArray_B argu1 = u0 ;
            DoubleArray_B argu2 = u ;
            DoubleArray_B argv1 = v0 ;
            DoubleArray_B argv2 = v ;
            project_fwd(argu1, argv1, argu2, argv2);
            ADStack.pushObjRef(argv2) ;
            ADStack.pushObjRef(argv1) ;
            ADStack.pushObjRef(argu2) ;
            ADStack.pushObjRef(argu1) ;
        }
        advect_fwd(1, u, u0, u0, v0, dt);
        advect_fwd(2, v, v0, u0, v0, dt);
        project_fwd(u, v, u0, v0);
    }

    void vel_step_bwd(DoubleArray_B u, DoubleArray_B v, DoubleArray_B u0, DoubleArray_B v0, double visc,
                  double dt) {
        int ii2 = 40 ;
        if (N<80) {
            ii2 = N/2 ;
        }
        project_bwd(u, v, u0, v0);
        advect_bwd(2, v, v0, u0, v0, dt);
        advect_bwd(1, u, u0, u0, v0, dt);
        {
            DoubleArray_B argu1 = (DoubleArray_B)ADStack.popObjRef() ;
            DoubleArray_B argu2 = (DoubleArray_B)ADStack.popObjRef() ;
            DoubleArray_B argv1 = (DoubleArray_B)ADStack.popObjRef() ;
            DoubleArray_B argv2 = (DoubleArray_B)ADStack.popObjRef() ;
            project_bwd(argu1, argv1, argu2, argv2);
            diffuse_bwd(2, visc, dt);
            argx0 = (DoubleArray_B)ADStack.popObjRef() ;
            argx = (DoubleArray_B)ADStack.popObjRef() ;
            diffuse_bwd(1, visc, dt);
            argx0 = (DoubleArray_B)ADStack.popObjRef() ;
            argx = (DoubleArray_B)ADStack.popObjRef() ;
        }
        add_source_bwd(v, v0, dt);
        add_source_bwd(u, u0, dt);
        timeControls = (ChainedList_B)ADStack.popObjRef() ;
        ((Double_B)timeControls.head).adj += u.adjs[INDEX(ii2,ii2)] ;
        u.vals[INDEX(ii2,ii2)] = ADStack.popDouble() ;
    }

    void dens_step_fwd(DoubleArray_B x, DoubleArray_B x0, DoubleArray_B u, DoubleArray_B v, double diff,
            double dt) {
        ADStack.pushObjRef(argx) ;
        argx = x0 ;
        ADStack.pushObjRef(argx0) ;
        argx0 = x ;
        ADStack.pushObjRef(argx) ;
        argx = x ;
        ADStack.pushObjRef(argx0) ;
        argx0 = x0 ;
    }

    void dens_step_bwd(DoubleArray_B x, DoubleArray_B x0, DoubleArray_B u, DoubleArray_B v, double diff,
            double dt) {
        argx0 = (DoubleArray_B)ADStack.popObjRef() ;
        argx = (DoubleArray_B)ADStack.popObjRef() ;
        argx0 = (DoubleArray_B)ADStack.popObjRef() ;
        argx = (DoubleArray_B)ADStack.popObjRef() ;
    }

    void add_source_fwd(DoubleArray_B x, DoubleArray_B s, double dt) {
        int i, size = (N + 2) * (N + 2);
        for (i = 0; i < size; ++i) {
            ADStack.pushDouble(x.vals[i]) ;
            x.vals[i] += dt * s.vals[i] ;
        }
    }

    void add_source_bwd(DoubleArray_B x, DoubleArray_B s, double dt) {
        int i, size = (N + 2) * (N + 2);
        for (i = size-1; i >= 0; --i) {
            x.vals[i] = ADStack.popDouble() ;
            s.adjs[i] += dt * x.adjs[i] ;
        }
    }

    void diffuse_fwd(int b, double diff, double dt) {
        int i, j, k;
        double a = dt * diff * N * N;
        for (k = 0; k < 20; ++k) {
            for (i = 1; i <= N; ++i) {
                for (j = 1; j <= N; ++j) {
                    ADStack.pushDouble(argx.vals[INDEX(i, j)]) ;
                    argx.vals[INDEX(i, j)] =
                        (argx0.vals[INDEX(i, j)]
                         + a * (argx.vals[INDEX(i - 1, j)] 
                                + argx.vals[INDEX(i + 1, j)]
                                + argx.vals[INDEX(i, j - 1)]
                                + argx.vals[INDEX(i, j + 1)]))
                        / (1 + 4 * a) ;
                }
            }
            set_bnd_fwd(b, argx);
        }
        ADStack.pushDouble(a) ;
    }

    void diffuse_bwd(int b, double diff, double dt) {
        int i, j, k;
        double a ;
        a = ADStack.popDouble() ;
        for (k = 19; k >=0 ; --k) {
            set_bnd_bwd(b, argx);
            for (i = N; i >= 1; --i) {
                for (j = N; j >= 1 ; --j) {
                    argx.vals[INDEX(i, j)] = ADStack.popDouble() ;
                    double tmpb = argx.adjs[INDEX(i, j)] / (1 + 4 * a) ;
                    double tmpb2 = a * tmpb ;
                    argx0.adjs[INDEX(i, j)] += tmpb ;
                    argx.adjs[INDEX(i - 1, j)] += tmpb2 ;
                    argx.adjs[INDEX(i + 1, j)] += tmpb2 ;
                    argx.adjs[INDEX(i, j - 1)] += tmpb2 ;
                    argx.adjs[INDEX(i, j + 1)] += tmpb2 ;
                    argx.adjs[INDEX(i, j)] = 0.0 ;
                }
            }
        }
    }

    void advect_fwd(int b, DoubleArray_B d, DoubleArray_B d0, DoubleArray_B u, DoubleArray_B v, double dt) {
        int i, j, i0=0, j0=0, i1=0, j1=0;
        double x, y, s0=0, t0=0, s1=0, t1=0, dt0;
        dt0 = dt * N;
        for (i = 1; i <= N; ++i) {
            for (j = 1; j <= N; ++j) {
                x = i - dt0 * u.vals[INDEX(i, j)];
                y = j - dt0 * v.vals[INDEX(i, j)];
                if (x < 0.5) {
                    x = 0.5;
                    ADStack.pushControl(0) ;
                } else
                    ADStack.pushControl(1) ;
                if (x > N + 0.5) {
                    x = N + 0.5;
                    ADStack.pushControl(0) ;
                } else
                    ADStack.pushControl(1) ;
                ADStack.pushInteger(i0) ;
                i0 = (int) x;
                ADStack.pushInteger(i1) ;
                i1 = i0 + 1;
                if (y < 0.5) {
                    y = 0.5;
                    ADStack.pushControl(0) ;
                } else
                    ADStack.pushControl(1) ;
                if (y > N + 0.5) {
                    y = N + 0.5;
                    ADStack.pushControl(0) ;
                } else
                    ADStack.pushControl(1) ;
                ADStack.pushInteger(j0) ;
                j0 = (int) y;
                ADStack.pushInteger(j1) ;
                j1 = j0 + 1;
                ADStack.pushDouble(s1) ;
                s1 = x - i0;
                ADStack.pushDouble(s0) ;
                s0 = 1 - s1;
                ADStack.pushDouble(t1) ;
                t1 = y - j0;
                ADStack.pushDouble(t0) ;
                t0 = 1 - t1;
                ADStack.pushDouble(d.vals[INDEX(i, j)]) ;
                d.vals[INDEX(i, j)] =
                    s0 * (t0 * d0.vals[INDEX(i0, j0)] + t1 * d0.vals[INDEX(i0, j1)])
                    + s1 * (t0 * d0.vals[INDEX(i1, j0)] + t1 * d0.vals[INDEX(i1, j1)]);
            }
        }
        set_bnd_fwd(b, d);
        ADStack.pushDouble(t1) ;
        ADStack.pushDouble(t0) ;
        ADStack.pushInteger(j1) ;
        ADStack.pushInteger(j0) ;
        ADStack.pushDouble(s1) ;
        ADStack.pushDouble(s0) ;
        ADStack.pushInteger(i1) ;
        ADStack.pushInteger(i0) ;
    }

    void advect_bwd(int b, DoubleArray_B d, DoubleArray_B d0,
                    DoubleArray_B u, DoubleArray_B v, double dt) {
        int i, j, i0, j0, i1, j1;
        double x, y, s0, t0, s1, t1, dt0;
        double xb, yb, s0b, t0b, s1b, t1b ;
        double tempb, tempb0 ;
        int branch ;
        i0 = ADStack.popInteger() ;
        i1 = ADStack.popInteger() ;
        s0 = ADStack.popDouble() ;
        s1 = ADStack.popDouble() ;
        j0 = ADStack.popInteger() ;
        j1 = ADStack.popInteger() ;
        t0 = ADStack.popDouble() ;
        t1 = ADStack.popDouble() ;
        set_bnd_bwd(b, d);
        dt0 = dt * N;
        for (i = N; i >= 1; --i) {
            for (j = N; j >= 1; --j) {
                d.vals[INDEX(i, j)] = ADStack.popDouble() ;
                s0b = (t0 * d0.vals[INDEX(i0, j0)] + t1 * d0.vals[INDEX(i0, j1)])
                    * d.adjs[INDEX(i, j)] ;
                tempb = s0 * d.adjs[INDEX(i, j)] ;
                s1b = (t0 * d0.vals[INDEX(i1, j0)] + t1 * d0.vals[INDEX(i1, j1)])
                    * d.adjs[INDEX(i, j)] - s0b ;
                tempb0 = s1 * d.adjs[INDEX(i, j)] ;
                d.adjs[INDEX(i, j)] = 0.0 ;
                t0b = d0.vals[INDEX(i1, j0)] * tempb0 + d0.vals[INDEX(i0, j0)] * tempb ;
                d0.adjs[INDEX(i1, j0)] += t0*tempb0 ;
                t1b = d0.vals[INDEX(i1, j1)] * tempb0 + d0.vals[INDEX(i0, j1)] * tempb - t0b ;
                d0.adjs[INDEX(i1, j1)] += t1*tempb0 ;
                d0.adjs[INDEX(i0, j0)] += t0*tempb ;
                d0.adjs[INDEX(i0, j1)] += t1*tempb ;
                t0 = ADStack.popDouble() ;
                t1 = ADStack.popDouble() ;
                yb = t1b ;
                s0 = ADStack.popDouble() ;
                s1 = ADStack.popDouble() ;
                xb = s1b ;
                j1 = ADStack.popInteger() ;
                j0 = ADStack.popInteger() ;
                branch = ADStack.popControl() ;
                if (branch==0) yb = 0.0 ;
                branch = ADStack.popControl() ;
                if (branch==0) yb = 0.0 ;
                i1 = ADStack.popInteger() ;
                i0 = ADStack.popInteger() ;
                branch = ADStack.popControl() ;
                if (branch==0) xb = 0.0 ;
                branch = ADStack.popControl() ;
                if (branch==0) xb = 0.0 ;
                v.adjs[INDEX(i, j)] -= dt0*yb ;
                u.adjs[INDEX(i, j)] -= dt0*xb ;
            }
        }
    }

    void project_fwd(DoubleArray_B u, DoubleArray_B v, DoubleArray_B p, DoubleArray_B div) {
        int i,j,k;
        double h, tmp ;
        h = 1.0 / N;
        for (i = 1; i <= N; ++i) {
            for (j = 1; j <= N; ++j) {
                ADStack.pushDouble(div.vals[INDEX(i, j)]) ;
                div.vals[INDEX(i, j)] = -0.5*h*
                    (u.vals[INDEX(i+1, j)] - u.vals[INDEX(i-1, j)]
                     + v.vals[INDEX(i, j+1)] - v.vals[INDEX(i, j-1)]) ;
                ADStack.pushDouble(p.vals[INDEX(i, j)]) ;
                p.vals[INDEX(i, j)] = 0.0 ;
            }
        }
        set_bnd_fwd(0, div);
        set_bnd_fwd(0, p);
        for (k = 0; k < 20; ++k) {
            for (i = 1; i <= N; ++i) {
                for (j = 1; j <= N; ++j) {
                    tmp = (div.vals[INDEX(i, j)]
                           + p.vals[INDEX(i-1, j)] + p.vals[INDEX(i+1, j)]
                           + p.vals[INDEX(i, j-1)] + p.vals[INDEX(i, j+1)]) / 4 ;
                    ADStack.pushDouble(p.vals[INDEX(i, j)]) ;
                    p.vals[INDEX(i, j)] = tmp ;
                }
            }
            set_bnd_fwd(0, p);
        }
        for (i = 1; i <= N; ++i) {
            for (j = 1; j <= N; ++j) {
                ADStack.pushDouble(u.vals[INDEX(i, j)]) ;
                u.vals[INDEX(i, j)] = u.vals[INDEX(i, j)]
                    - 0.5*(p.vals[INDEX(i+1, j)] - p.vals[INDEX(i-1, j)])/h ;
                ADStack.pushDouble(v.vals[INDEX(i, j)]) ;
                v.vals[INDEX(i, j)] = v.vals[INDEX(i, j)]
                    - 0.5*(p.vals[INDEX(i, j+1)] - p.vals[INDEX(i, j-1)])/h ;
            }
        }
        set_bnd_fwd(1, u);
        set_bnd_fwd(2, v);
    }

    void project_bwd(DoubleArray_B u, DoubleArray_B v, DoubleArray_B p, DoubleArray_B div) {
        int i, j, k;
        double h;
        double tempb ;
        double tmpb ;
        set_bnd_bwd(2, v);
        set_bnd_bwd(1, u);
        h = 1.0 / N;
        for (i = N; i >= 1; --i) {
            for (j = N; j >= 1; --j) {
                v.vals[INDEX(i, j)] = ADStack.popDouble() ;
                tempb = -0.5*v.adjs[INDEX(i, j)]/h ;
                p.adjs[INDEX(i, j + 1)] += tempb ;
                p.adjs[INDEX(i, j - 1)] -= tempb ;
                u.vals[INDEX(i, j)] = ADStack.popDouble() ;
                tempb = -0.5*u.adjs[INDEX(i, j)]/h ;
                p.adjs[INDEX(i + 1, j)] += tempb ;
                p.adjs[INDEX(i - 1, j)] -= tempb ;
            }
        }
        for (k = 19; k >= 0; --k) {
            set_bnd_bwd(0, p) ;
            for (i = N; i >= 1; --i) {
                for (j = N; j >= 1; --j) {
                    p.vals[INDEX(i, j)] = ADStack.popDouble() ;
                    tmpb = p.adjs[INDEX(i, j)] ;
                    p.adjs[INDEX(i, j)] = 0.0 ;
                    tempb = tmpb/4 ;
                    div.adjs[INDEX(i, j)] += tempb ;
                    p.adjs[INDEX(i - 1, j)] += tempb ;
                    p.adjs[INDEX(i + 1, j)] += tempb ;
                    p.adjs[INDEX(i, j - 1)] += tempb ;
                    p.adjs[INDEX(i, j + 1)] += tempb ;
                }
            }
        }
        set_bnd_bwd(0, p) ;
        set_bnd_bwd(0, div) ;
        for (i = N; i >= 1; --i) {
            for (j = N; j >= 1; --j) {
                p.vals[INDEX(i, j)] = ADStack.popDouble() ;
                p.adjs[INDEX(i, j)] = 0.0 ;
                div.vals[INDEX(i, j)] = ADStack.popDouble() ;
                tempb = -h*0.5*div.adjs[INDEX(i, j)] ;
                div.adjs[INDEX(i, j)] = 0.0 ;
                u.adjs[INDEX(i + 1, j)] += tempb ;
                u.adjs[INDEX(i - 1, j)] -= tempb ;
                v.adjs[INDEX(i, j + 1)] += tempb ;
                v.adjs[INDEX(i, j - 1)] -= tempb ;
            }
        }
    }

    void set_bnd_fwd(int b, DoubleArray_B x) {
        int i;
        double tmp;
        for (i = 1; i <= N; ++i) {
            if (b==1) {
                tmp = -x.vals[INDEX(1, i)] ;
                ADStack.pushDouble(x.vals[INDEX(0, i)]) ;
                x.vals[INDEX(0, i)] = tmp ;
                ADStack.pushControl(0) ;
            } else {
                tmp = x.vals[INDEX(1, i)] ;
                ADStack.pushDouble(x.vals[INDEX(0, i)]) ;
                x.vals[INDEX(0, i)] = tmp ;
                ADStack.pushControl(1) ;
            }
            if (b==1) {
                tmp = -x.vals[INDEX(N, i)] ;
                ADStack.pushDouble(x.vals[INDEX(N+1, i)]) ;
                x.vals[INDEX(N+1, i)] = tmp ;
                ADStack.pushControl(0) ;
            } else {
                tmp = x.vals[INDEX(N, i)] ;
                ADStack.pushDouble(x.vals[INDEX(N+1, i)]) ;
                x.vals[INDEX(N+1, i)] = tmp ;
                ADStack.pushControl(1) ;
            }
            if (b==2) {
                tmp = -x.vals[INDEX(i, 1)] ;
                ADStack.pushDouble(x.vals[INDEX(i, 0)]) ;
                x.vals[INDEX(i, 0)] = tmp ;
                ADStack.pushControl(0) ;
            } else {
                tmp = x.vals[INDEX(i, 1)] ;
                ADStack.pushDouble(x.vals[INDEX(i, 0)]) ;
                x.vals[INDEX(i, 0)] = tmp ;
                ADStack.pushControl(1) ;
            }
            if (b==2) {
                tmp = -x.vals[INDEX(i, N)] ;
                ADStack.pushDouble(x.vals[INDEX(i, N+1)]) ;
                x.vals[INDEX(i, N+1)] = tmp ;
                ADStack.pushControl(1) ;
            } else {
                tmp = x.vals[INDEX(i, N)] ;
                ADStack.pushDouble(x.vals[INDEX(i, N+1)]) ;
                x.vals[INDEX(i, N+1)] = tmp ;
                ADStack.pushControl(0) ;
            }
        }
        tmp = 0.5 * (x.vals[INDEX(1, 0)] + x.vals[INDEX(0, 1)]);
        ADStack.pushDouble(x.vals[INDEX(0, 0)]) ;
        x.vals[INDEX(0, 0)] = tmp ;
        tmp = 0.5 * (x.vals[INDEX(1, N + 1)] + x.vals[INDEX(0, N)]);
        ADStack.pushDouble(x.vals[INDEX(0, N + 1)]) ;
        x.vals[INDEX(0, N + 1)] = tmp ;
        tmp = 0.5 * (x.vals[INDEX(N, 0)] + x.vals[INDEX(N + 1, 1)]);
        ADStack.pushDouble(x.vals[INDEX(N + 1, 0)]) ;
        x.vals[INDEX(N + 1, 0)] = tmp ;
        tmp = 0.5 * (x.vals[INDEX(N, N + 1)] + x.vals[INDEX(N + 1, N)]);
        ADStack.pushDouble(x.vals[INDEX(N + 1, N + 1)]) ;
        x.vals[INDEX(N + 1, N + 1)] = tmp ;
    }

    void set_bnd_bwd(int b, DoubleArray_B x) {
        int i;
        double tmpb ;
        int branch ;
        x.vals[INDEX(N + 1, N + 1)] = ADStack.popDouble() ;
        tmpb = x.adjs[INDEX(N + 1, N + 1)] ;
        x.adjs[INDEX(N + 1, N + 1)] = 0.0 ;
        x.adjs[INDEX(N, N + 1)] += 0.5*tmpb ;
        x.adjs[INDEX(N + 1, N)] += 0.5*tmpb ;
        x.vals[INDEX(N + 1, 0)] = ADStack.popDouble() ;
        tmpb = x.adjs[INDEX(N + 1, 0)] ;
        x.adjs[INDEX(N + 1, 0)] = 0.0 ;
        x.adjs[INDEX(N, 0)] += 0.5*tmpb ;
        x.adjs[INDEX(N + 1, 1)] += 0.5*tmpb ;
        x.vals[INDEX(0, N + 1)] = ADStack.popDouble() ;
        tmpb = x.adjs[INDEX(0, N + 1)] ;
        x.adjs[INDEX(0, N + 1)] = 0.0 ;
        x.adjs[INDEX(1, N + 1)] += 0.5*tmpb ;
        x.adjs[INDEX(0, N)] += 0.5*tmpb ;
        x.vals[INDEX(0, 0)] = ADStack.popDouble() ;
        tmpb = x.adjs[INDEX(0, 0)] ;
        x.adjs[INDEX(0, 0)] = 0.0 ;
        x.adjs[INDEX(1, 0)] += 0.5*tmpb ;
        x.adjs[INDEX(0, 1)] += 0.5*tmpb ;
        for (i = N; i >= 1; --i) {
            branch = ADStack.popControl() ;
            if (branch==0) {
                x.vals[INDEX(i, N+1)] = ADStack.popDouble() ;
                tmpb = x.adjs[INDEX(i, N+1)] ;
                x.adjs[INDEX(i, N+1)] = 0.0 ;
                x.adjs[INDEX(i, N)] += tmpb ;
            } else {
                x.vals[INDEX(i, N+1)] = ADStack.popDouble() ;
                tmpb = x.adjs[INDEX(i, N+1)] ;
                x.adjs[INDEX(i, N+1)] = 0.0 ;
                x.adjs[INDEX(i, N)] -= tmpb ;
            }
            branch = ADStack.popControl() ;
            if (branch==0) {
                x.vals[INDEX(i, 0)] = ADStack.popDouble() ;
                tmpb = x.adjs[INDEX(i, 0)] ;
                x.adjs[INDEX(i, 0)] = 0.0 ;
                x.adjs[INDEX(i, 1)] -= tmpb ;
            } else {
                x.vals[INDEX(i, 0)] = ADStack.popDouble() ;
                tmpb = x.adjs[INDEX(i, 0)] ;
                x.adjs[INDEX(i, 0)] = 0.0 ;
                x.adjs[INDEX(i, 1)] += tmpb ;
            }
            branch = ADStack.popControl() ;
            if (branch==0) {
                x.vals[INDEX(N+1, i)] = ADStack.popDouble() ;
                tmpb = x.adjs[INDEX(N+1, i)] ;
                x.adjs[INDEX(N+1, i)] = 0.0 ;
                x.adjs[INDEX(N, i)] -= tmpb ;
            } else {
                x.vals[INDEX(N+1, i)] = ADStack.popDouble() ;
                tmpb = x.adjs[INDEX(N+1, i)] ;
                x.adjs[INDEX(N+1, i)] = 0.0 ;
                x.adjs[INDEX(N, i)] += tmpb ;
            }
            branch = ADStack.popControl() ;
            if (branch==0) {
                x.vals[INDEX(0, i)] = ADStack.popDouble() ;
                tmpb = x.adjs[INDEX(0, i)] ;
                x.adjs[INDEX(0, i)] = 0.0 ;
                x.adjs[INDEX(1, i)] -= tmpb ;
            } else {
                x.vals[INDEX(0, i)] = ADStack.popDouble() ;
                tmpb = x.adjs[INDEX(0, i)] ;
                x.adjs[INDEX(0, i)] = 0.0 ;
                x.adjs[INDEX(1, i)] += tmpb ;
            }
        }
    }

    final int INDEX(int i, int j) {
        return i + (N + 2) * j;
    }

}
