import java.io.*;

/** A pair of an double array variable and their tangent derivatives.
 * Features the ADRestorable interface */
public class DoubleArray_B implements ADRestorable, AutoCloseable {

    // Note: I don't know how I can catch the finalize() "event" for arrays
    // such as the two below. Therefore I am just hoping that finalize() will be
    // called on an instance of this class before it is called on the arrays
    // arrays inside it, (or not too long afterwards), so that I can still
    // save both the instance and the two arrays inside it.

    public double[] vals ;
    public double[] adjs ;
    private int objectKey = -1 ;

    public DoubleArray_B() {
        this.objectKey = ADStack.registerObject(this) ;
        vals = null ;
        adjs = null ;
    }

    public DoubleArray_B(int size) {
        this.objectKey = ADStack.registerObject(this) ;
        vals = new double[size] ;
        adjs = new double[size] ;
    }

    public void setObjectKey(int objectKey) {
        this.objectKey = objectKey ;
    }

    public int objectKey() {
        return objectKey ;
    }

    public void finalize() {
// System.out.println("  FINALIZING "+this) ;
        if (objectKey!=-1) ADStack.unRegisterOneObject(objectKey, this) ;
    }

    public void close() {
// System.out.println("     CLOSING "+this) ;
        if (objectKey!=-1) ADStack.unRegisterOneObject(objectKey, this) ;
    }

    public void storeFields() {
        for (int i=0 ; i<vals.length ; ++i)
            ADStack.plainPushDouble(vals[i]) ;
        ADStack.plainPushInteger(vals.length) ;
    }

    public void restoreFields() {
        int size = ADStack.plainPopInteger() ;
        vals = new double[size] ;
        adjs = new double[size] ;
        for (int i=vals.length-1 ; i>=0 ; --i) {
            vals[i] = ADStack.plainPopDouble() ;
            adjs[i] = 0.0 ;
        }
    }

    public void restoreObjectField(int fieldIndex, Object destination) {
        // No Object field in a DoubleArray_B!
    }

    public String toString() {return "[DoubleArray_B:"+objectKey+"]";}

}
