public class ChainedList {
    public Object head ;
    public ChainedList tail ;

    public ChainedList(Object head, ChainedList tail) {
        this.head = head ;
        this.tail = tail ;
    }

    public String toString() {return toStringRec(null) ;}

    private String toStringRec(ChainedList dejaVu) {
        int cycleRank = (dejaVu==null?-1:dejaVu.rank(this)) ;
        if (cycleRank!=-1) {
            return "CYCLE-"+(cycleRank+1) ;
        } else {
            ChainedList listTail = this;
            String result = null ;
            cycleRank = -1 ;
            while (listTail != null && (cycleRank = dejaVu == null ? -1 : dejaVu.rank(listTail)) == -1) {
                result = (result==null?"(":result+" ") ;
                dejaVu = new ChainedList(listTail, dejaVu) ;
                if (listTail.head instanceof ChainedList) {
                    result = result + ((ChainedList)listTail.head).toStringRec(dejaVu) ;
                } else {
                    result = result + listTail.head;
                }
                listTail = listTail.tail;
            }
            if (listTail!=null)
                result = result+" . "+(cycleRank==-1?listTail:"CYCLE-"+(cycleRank+1));
            result = result + ")";
            return result;
        }
    }

    public int rank(Object obj) {
        int rank = 0;
        ChainedList inList = this;
        while (inList!=null && inList.head!=obj) {
            ++rank ;
            inList = inList.tail ;
        }
        return (inList==null?-1:rank) ;
    }

}
