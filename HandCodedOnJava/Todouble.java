/** A container for a double */
public class Todouble {
    private double contents = 0.0 ;

    public double get() {return contents;}

    public void set(double x) {contents = x;}
}
