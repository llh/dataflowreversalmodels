public class ChainedList_D {
    public Object head ;
    public ChainedList_D tail ;

    public ChainedList_D(Object head, ChainedList_D tail) {
        this.head = head ;
        this.tail = tail ;
    }
}
