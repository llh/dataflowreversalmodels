import java.io.* ;
import java.util.StringTokenizer ;

public class Driver{

    // Size of the test case
    static int N = 80;
    static int nbTicks = 20;

    // The individual element of the gradient that we want to test.
    // -1 means use the "condensed" method
    // i from 0 to 81 means test on cu[i]
    // i from 82 to 163 means test on cv[i-82]
    // 164 means test on ct
    static int gradelem = -1 ;

    // For condensing derivatives into one scalar:
    static double seed = 0.87 ;
    static double curSeed = 0.0 ;
    static double nextCurSeed() {
        curSeed += seed ;
        if (curSeed>=1.0) curSeed-=1.0 ;
        return curSeed+1.0 ;
    }

    public static void main(String args[]) {
        if (args.length > 0) {
            N = Integer.parseInt(args[0]) ;
        }
        if (args.length > 1) {
            nbTicks = Integer.parseInt(args[1]) ;
        }
        if (args.length > 2) {
            gradelem = Integer.parseInt(args[2]) ;
        }
        int SIZE = (N + 2) * (N + 2);
        double dt=0.01, visc=1.0E-4, diff=1.0E-4 ;
        double[] u0 = new double[SIZE];
        double[] v0 = new double[SIZE];
        double[] u0_prev = new double[SIZE];
        double[] v0_prev = new double[SIZE];
        double[] dense0 = new double[SIZE];
        double[] dense0_prev = new double[SIZE];
        long startTime, endTime ;
        int lastReadFromFile = 0 ;
        try {
            BufferedReader reader = new BufferedReader(new FileReader("../DATA"));
            StringTokenizer st ;
            String line = reader.readLine() ; // dt visc diff
            st = new StringTokenizer(line) ;
            dt = Double.parseDouble(st.nextToken()) ;
            visc = Double.parseDouble(st.nextToken()) ;
            diff = Double.parseDouble(st.nextToken()) ;
            int i = 0 ;
            while (line != null && i<SIZE) {
                line = reader.readLine();     // u u_prev v v_prev dens dens_prev
                if (line!=null) {
                    st = new StringTokenizer(line) ;
                    u0[i] = Double.parseDouble(st.nextToken()) ;
                    u0_prev[i] = Double.parseDouble(st.nextToken()) ;
                    v0[i] = Double.parseDouble(st.nextToken()) ;
                    v0_prev[i] = Double.parseDouble(st.nextToken()) ;
                    dense0[i] = Double.parseDouble(st.nextToken()) ;
                    dense0_prev[i] = Double.parseDouble(st.nextToken()) ;
                    ++i ;
                }
            }
            lastReadFromFile = i-1 ;
        } catch (Exception e) {
            System.out.println("EXCEPTION WHILE READING DATA "+e) ;
        }
        // Replicate data when test size N/SIZE is larger than data size (80/6724):
        for (int i=lastReadFromFile+1 ; i<SIZE ; ++i) {
            u0[i] = u0[i-6724] ;
            u0_prev[i] = u0_prev[i-6724] ;
            v0[i] = v0[i-6724] ;
            v0_prev[i] = v0_prev[i-6724] ;
            dense0[i] = dense0[i-6724] ;
            dense0_prev[i] = dense0_prev[i-6724] ;
        }
        double[] cu = new double[82] ;
        double[] cv = new double[82] ;
        for (int i=0 ; i<82 ; ++i) {
            cu[i] = 0.0 ;
            cv[i] = 0.0 ;
        }
        double ct = 0.0 ;
        Solver solver = new Solver(N);
        for (int i=0 ; i<SIZE ; ++i) {
            solver.u[i] = u0[i] ;
            solver.u_prev[i] = u0_prev[i] ;
            solver.v[i] = v0[i] ;
            solver.v_prev[i] = v0_prev[i] ;
            solver.dense[i] = dense0[i] ;
            solver.dense_prev[i] = dense0_prev[i] ;
        }
        startTime = System.currentTimeMillis();
        double cost = simulate(solver,dt,visc,diff,cu,cv,ct) ;
        endTime = System.currentTimeMillis();
        System.out.println("  (with Java) RESULT:  "+cost) ;
        System.out.println("  Problem size: Mesh "+N+"*"+N+", "+nbTicks+" time steps") ;
        System.out.println("  test gradient "+(gradelem<0 ? "condensed" : "elem "+gradelem)) ;
        System.out.println(" Time of  primal:"+(endTime-startTime)+" millisec") ;

        // DIVIDED DIFFERENCES IN DIRECTION : cudir, cvdir, ctdir

        ct = 0 ;
        double[] cudir = new double[82] ;
        double[] cvdir = new double[82] ;
        double ctdir = 0.0 ;
        double costdir = 0.0 ;
        for (int i=0 ; i<82 ; ++i) {
            cu[i] = 0.0 ;
            cudir[i] = 0.0 ;
        }
        for (int i=0 ; i<82 ; ++i) {
            cv[i] = 0.0 ;
            cvdir[i] = 0.0 ;
        }

        if (gradelem<0) { // use "condensation"
            curSeed = 0.0 ;
            ctdir =  nextCurSeed() ;
            for (int i=0 ; i<82 ; ++i) {
                cudir[i] = nextCurSeed() ;
            }
            for (int i=0 ; i<82 ; ++i) {
                cvdir[i] = nextCurSeed() ;
            }
            curSeed = 0.0 ;
            costdir = nextCurSeed() ;
        } else {
            if (gradelem<82) {
                cudir[gradelem] = 1.0 ;
            } else if (gradelem<164) {
                cvdir[gradelem-82] = 1.0 ;
            } else {
                ctdir = 1.0 ;
            }
            costdir = 1.0 ;
        }

        double eps = 1.0e-8 ;
        for (int i=0 ; i<82 ; ++i) {
            cu[i] += eps*cudir[i] ;
            cv[i] += eps*cvdir[i] ;
        }
        ct += eps*ctdir ;
        for (int i=0 ; i<SIZE ; ++i) {
            solver.u[i] = u0[i] ;
            solver.u_prev[i] = u0_prev[i] ;
            solver.v[i] = v0[i] ;
            solver.v_prev[i] = v0_prev[i] ;
            solver.dense[i] = dense0[i] ;
            solver.dense_prev[i] = dense0_prev[i] ;
        }
        double cost_eps = simulate(solver,dt,visc,diff,cu,cv,ct) ;
        System.out.println((gradelem<0?"[seed:"+seed:"[element "+gradelem)+"] Condensed perturbed result : "+costdir*cost_eps+" (epsilon="+eps+")");

        // TEST TANGENT DIFF IN DIRECTION : cudir, cvdir, ctdir
        Solver_D solverd = new Solver_D(N);
        for (int i=0 ; i<SIZE ; ++i) {
            solverd.u.vals[i] = u0[i] ;
            solverd.u.tgts[i] = 0.0 ;
            solverd.u_prev.vals[i] = u0_prev[i] ;
            solverd.u_prev.tgts[i] = 0.0 ;
            solverd.v.vals[i] = v0[i] ;
            solverd.v.tgts[i] = 0.0 ;
            solverd.v_prev.vals[i] = v0_prev[i] ;
            solverd.v_prev.tgts[i] = 0.0 ;
            solverd.dense.vals[i] = dense0[i] ;
            solverd.dense.tgts[i] = 0.0 ;
            solverd.dense_prev.vals[i] = dense0_prev[i] ;
            solverd.dense_prev.tgts[i] = 0.0 ;
        }
        DoubleArray_D cud = new DoubleArray_D(82) ;
        DoubleArray_D cvd = new DoubleArray_D(82) ;
        for (int i=0 ; i<82 ; ++i) {
            cud.vals[i] = 0.0 ;
            cud.tgts[i] = cudir[i] ;
            cvd.vals[i] = 0.0 ;
            cvd.tgts[i] = cvdir[i] ;
        }
        Todouble toCost = new Todouble() ;
        startTime = System.currentTimeMillis();
        double costd = simulate_d(solverd,dt,visc,diff,cud,cvd,0.0,ctdir,toCost) ;
        endTime = System.currentTimeMillis();
        System.out.println((gradelem<0?"[seed:"+seed:"[element "+gradelem)+"] Condensed result           : "+costdir*toCost.get()+" (epsilon="+eps+")") ;
        double condFD = costdir*(cost_eps-cost)/eps ;
        System.out.println((gradelem<0?"[seed:"+seed:"[element "+gradelem)+"] Condensed FD     : " + condFD) ;
        double condTGT = costdir*costd ;
        System.out.println((gradelem<0?"[seed:"+seed:"[element "+gradelem)+"] Condensed tangent: "+condTGT+" FD err:"+100.0*(condFD-condTGT)/condFD) ;
        System.out.println(" Time of tangent:"+(endTime-startTime)+" millisec") ;


        // TEST ADJOINT :
        Solver_B solverb = new Solver_B(N);
        for (int i=0 ; i<SIZE ; ++i) {
            solverb.u.vals[i] = u0[i] ;
            solverb.u.adjs[i] = 0.0 ;
            solverb.u_prev.vals[i] = u0_prev[i] ;
            solverb.u_prev.adjs[i] = 0.0 ;
            solverb.v.vals[i] = v0[i] ;
            solverb.v.adjs[i] = 0.0 ;
            solverb.v_prev.vals[i] = v0_prev[i] ;
            solverb.v_prev.adjs[i] = 0.0 ;
            solverb.dense.vals[i] = dense0[i] ;
            solverb.dense.adjs[i] = 0.0 ;
            solverb.dense_prev.vals[i] = dense0_prev[i] ;
            solverb.dense_prev.adjs[i] = 0.0 ;
        }
        DoubleArray_B cub = new DoubleArray_B(82) ;
        DoubleArray_B cvb = new DoubleArray_B(82) ;
        for (int i=0 ; i<82 ; ++i) {
            cub.vals[i] = 0.0 ;
            cub.adjs[i] = 0.0 ;
            cvb.vals[i] = 0.0 ;
            cvb.adjs[i] = 0.0 ;
        }
        Todouble ctb = new Todouble() ;
        double costb = 1.0 ;
        startTime = System.currentTimeMillis();
        simulate_b(solverb,dt,visc,diff,cub,cvb,0.0,ctb,costb) ;
        endTime = System.currentTimeMillis();
        double adjcondense = 0.0 ;
        adjcondense += ctb.get()*ctdir ;
        for (int i=0 ; i<82 ; ++i) {
            adjcondense += cub.adjs[i]*cudir[i];
            adjcondense += cvb.adjs[i]*cvdir[i] ;
        }
        adjcondense = adjcondense*costdir ;
        ADStack.unRegisterObject(cvb) ;
        ADStack.unRegisterObject(cub) ;
        solverb.new_bwd() ;
        System.out.println((gradelem<0?"[seed:"+seed:"[element "+gradelem)+"] Condensed adjoint: "+adjcondense+" diff with tgt:"+100.0*(adjcondense-condTGT)/adjcondense) ;
        System.out.println(" Time of adjoint:"+(endTime-startTime)+" millisec") ;
        ADStack.showPeakSize() ;
    }

    public static double simulate(Solver solver, double dt, double visc, double diff,
                                  double cu[], double cv[], double ct) {
        solver.applyControl(cu,cv) ;
        solver.ticks(nbTicks, dt, visc, diff, ct) ;
        return solver.computeCost() ;
    }

    public static double simulate_d(Solver_D solver, double dt, double visc, double diff,
                                    DoubleArray_D cu, DoubleArray_D cv, double ct, double ctd, Todouble simulate) {
        solver.applyControl_d(cu,cv) ;
        solver.ticks_d(nbTicks, dt, visc, diff, ct, ctd) ;
        return solver.computeCost_d(simulate) ;
    }

    public static void simulate_b(Solver_B solver, double dt, double visc, double diff,
                                  DoubleArray_B cu, DoubleArray_B cv, double ct, Todouble ctb, double costb) {
        solver.applyControl_fwd(cu,cv) ;
        solver.ticks_fwd(nbTicks, dt, visc, diff, ct) ;

        solver.computeCost_b(costb) ;

// Uncomment next lines to force a GC at turn point
// System.out.println("CALLING GC") ;
        System.gc() ;
        System.runFinalization() ;

        solver.ticks_bwd(nbTicks, dt, visc, diff, ctb) ;
        solver.applyControl_bwd(cu,cv) ;
    }
}
