#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "Solver.h"

extern int N ;
extern int SIZE ;
extern int nbTicks ;
extern double *u ;
extern double *v ;
extern double *u_prev ;
extern double *v_prev ;
extern double *dense ;
extern double *dense_prev ;
extern ChainedList *timeControls ;
extern double *argx ;
extern double *argx0 ;

double simulate(double dt, double visc, double diff,
                double *cu, double *cv, double ct) ;

int main(int argc, char *argv[]) {
  N = 80 ;
  if (argc > 1) {
    N = atoi(argv[1]) ;
  }
  nbTicks = 20 ;
  if (argc > 2) {
    nbTicks = atoi(argv[2]) ;
  }

  double dt=0.01, visc=0.0001, diff=0.0001;
  FILE* datafile ;
  char *skippedLine ;
  int i, status ;
  double readu, readuprev, readv, readvprev, readdens, readdensprev ;

  clock_t startTime, endTime ;

  SIZE = (N + 2) * (N + 2) ;
  u = (double*)malloc(SIZE*sizeof(double)) ;
  v = (double*)malloc(SIZE*sizeof(double)) ;
  u_prev = (double*)malloc(SIZE*sizeof(double)) ;
  v_prev = (double*)malloc(SIZE*sizeof(double)) ;
  dense = (double*)malloc(SIZE*sizeof(double)) ;
  dense_prev = (double*)malloc(SIZE*sizeof(double)) ;

  int lastReadFromFile = 0 ;
  datafile = fopen("../DATA", "r") ;
  skippedLine = (char*)malloc(80) ;
  fscanf(datafile, "%[^\n]\n", skippedLine) ;
  i = 0 ;
  status = 1 ;
  while (status != EOF && i<SIZE) {
    status = fscanf(datafile, "%lf %lf %lf %lf %lf %lf\n",
                    &readu, &readuprev, &readv, &readvprev,
                    &readdens, &readdensprev) ;
    if (status != EOF) {
      u[i] = readu ;
      v[i] = readv ;
      u_prev[i] = readuprev ;
      v_prev[i] = readvprev ;
      dense[i] = readdens ;
      dense_prev[i] = readdensprev ;
      ++i ;
    }
  }
  lastReadFromFile = i-1 ;
  fclose(datafile) ;
  // Replicate data when test size N/SIZE is larger than data size (80/6724):
  for (int i=lastReadFromFile+1 ; i<SIZE ; ++i) {
    u[i] = u[i-6724] ;
    u_prev[i] = u_prev[i-6724] ;
    v[i] = v[i-6724] ;
    v_prev[i] = v_prev[i-6724] ;
    dense[i] = dense[i-6724] ;
    dense_prev[i] = dense_prev[i-6724] ;
  }

  double cu[82] ;
  double cv[82] ;
  for (i=0 ; i<82 ; ++i) {
    cu[i] = 0.0 ;
    cv[i] = 0.0 ;
  }
  double ct = 0.0 ;

  startTime = clock() ;
  double cost = simulate(dt, visc, diff, cu, cv, ct) ;
  endTime = clock() ;
  printf("  (with C) RAW RESULT: %18.15f\n", cost) ;
  printf("  Problem size: Mesh %i*%i, %i time steps\n", N, N, nbTicks) ;
  printf("  Runtime: %lf s.\n", (double)(endTime-startTime)/CLOCKS_PER_SEC) ;
}

double simulate(double dt, double visc, double diff,
                double *cu, double *cv, double ct) {
  applyControl(cu, cv) ;
  ticks(nbTicks, dt, visc, diff, ct) ;
  return computeCost() ;
}
