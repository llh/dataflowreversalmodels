/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.16 (feature_llhTests) - 11 Aug 2022 17:24
*/
#include <adContext.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "Solver_d.h"
extern int N;
extern int SIZE;
extern int nbTicks;
extern double *u;
extern double *ud;
extern double *v;
extern double *vd;
extern double *u_prev;
extern double *u_prevd;
extern double *v_prev;
extern double *v_prevd;
extern double *dense;
extern double *densed;
extern double *dense_prev;
extern double *dense_prevd;
extern ChainedList *timeControls;
extern ChainedList *timeControlsd;
extern double *argx;
extern double *argxd;
extern double *argx0;
extern double *argx0d;
double simulate(double dt, double visc, double diff, double *cu, double *cv, 
    double ct);
double simulate_d(double dt, double visc, double diff, double *cu, double *cud
    , double *cv, double *cvd, double ct, double ctd, double *simulate);

/*
  Differentiation of main as a context to call tangent code (with options context fixinterface no!refineADMM duplicable(INDEX)):
   Plus diff mem management of: alloc(*newtimeControls).tail:in-out
                v_prev:in-out u:in-out v:in-out dense_prev:in-out
                timeControls:in-out *timeControls.tail:in-out
                u_prev:in-out argx0:in-out dense:in-out argx:in-out
*/
int main(int argc, char *argv[]) {
    int ii1;
    N = 80;
    if (argc > 1)
        N = atoi(argv[1]);
    nbTicks = 20;
    if (argc > 2)
        nbTicks = atoi(argv[2]);
    double dt = 0.01;
    double visc = 0.0001;
    double diff = 0.0001;
    FILE *datafile;
    char *skippedLine;
    int i, status;
    double readu, readuprev, readv, readvprev, readdens, readdensprev;
    clock_t startTime, endTime;
    SIZE = (N+2)*(N+2);
    ud = (double *)malloc(SIZE*sizeof(double));
    for (ii1 = 0; ii1 < SIZE; ++ii1)
        ud[ii1] = 0.0;
    u = (double *)malloc(SIZE*sizeof(double));
    vd = (double *)malloc(SIZE*sizeof(double));
    for (ii1 = 0; ii1 < SIZE; ++ii1)
        vd[ii1] = 0.0;
    v = (double *)malloc(SIZE*sizeof(double));
    u_prevd = (double *)malloc(SIZE*sizeof(double));
    for (ii1 = 0; ii1 < SIZE; ++ii1)
        u_prevd[ii1] = 0.0;
    u_prev = (double *)malloc(SIZE*sizeof(double));
    v_prevd = (double *)malloc(SIZE*sizeof(double));
    for (ii1 = 0; ii1 < SIZE; ++ii1)
        v_prevd[ii1] = 0.0;
    v_prev = (double *)malloc(SIZE*sizeof(double));
    densed = (double *)malloc(SIZE*sizeof(double));
    for (ii1 = 0; ii1 < SIZE; ++ii1)
        densed[ii1] = 0.0;
    dense = (double *)malloc(SIZE*sizeof(double));
    dense_prevd = (double *)malloc(SIZE*sizeof(double));
    for (ii1 = 0; ii1 < SIZE; ++ii1)
        dense_prevd[ii1] = 0.0;
    dense_prev = (double *)malloc(SIZE*sizeof(double));
    int lastReadFromFile = 0;
    datafile = fopen("../DATA", "r");
    skippedLine = (char *)malloc(80);
    fscanf(datafile, "%[^\n]\n", skippedLine);
    i = 0;
    status = 1;
    while(status != -1 && i < SIZE) {
        status = fscanf(datafile, "%lf %lf %lf %lf %lf %lf\n", &readu, &
                        readuprev, &readv, &readvprev, &readdens, &
                        readdensprev);
        if (status != -1) {
            u[i] = readu;
            v[i] = readv;
            u_prev[i] = readuprev;
            v_prev[i] = readvprev;
            dense[i] = readdens;
            dense_prev[i] = readdensprev;
            ++i;
        }
    }
    lastReadFromFile = i - 1;
    fclose(datafile);
    // Replicate data when test size N/SIZE is larger than data size (80/6724):
    for (int i = lastReadFromFile + 1; i < SIZE; ++i) {
        u[i] = u[i - 6724];
        u_prev[i] = u_prev[i - 6724];
        v[i] = v[i - 6724];
        v_prev[i] = v_prev[i - 6724];
        dense[i] = dense[i - 6724];
        dense_prev[i] = dense_prev[i - 6724];
    }
    double cu[82];
    double cud[82];
    double cv[82];
    double cvd[82];
    for (i = 0; i < 82; ++i) {
        cu[i] = 0.0;
        cv[i] = 0.0;
    }
    double ct = 0.0;
    double ctd;
    startTime = clock();
    double cost;
    double costd;
    adContextTgt_init(1.e-8, 0.87);
    adContextTgt_initReal8("ct", &ct, &ctd);
    adContextTgt_initReal8Array("cu", cu, cud, 82);
    adContextTgt_initReal8Array("cv", cv, cvd, 82);
    costd = simulate_d(dt, visc, diff, cu, cud, cv, cvd, ct, ctd, &cost);
    adContextTgt_startConclude();
    adContextTgt_concludeReal8("cost", cost, costd);
    adContextTgt_conclude();
    endTime = clock();
    printf("  (with C) RAW RESULT: %18.15f\n", cost);
    printf("  Problem size: Mesh %i*%i, %i time steps\n", N, N, nbTicks);
    printf("  Runtime: %lf s.\n", ((double)(endTime-startTime)/(__clock_t)
           1000000));
}

/*
  Differentiation of simulate in forward (tangent) mode (with options context fixinterface no!refineADMM duplicable(INDEX)):
   variations   of useful results: simulate
   with respect to varying inputs: ct *cu *cv
   RW status of diff variables: *v_prev:(loc) *u:(loc) *v:(loc)
                *u_prev:(loc) simulate:out ct:in cu:(loc) *cu:in
                cv:(loc) *cv:in
   Plus diff mem management of: alloc(*newtimeControls).tail:in-out
                v_prev:in u:in v:in dense_prev:in timeControls:out
                u_prev:in argx0:in-out dense:in argx:in-out cu:in
                cv:in
*/
double simulate_d(double dt, double visc, double diff, double *cu, double *cud
        , double *cv, double *cvd, double ct, double ctd, double *simulate) {
    double result1;
    double result1d;
    applyControl_d(cu, cud, cv, cvd);
    ticks_d(nbTicks, dt, visc, diff, ct, ctd);
    result1d = computeCost_d(&result1);
    *simulate = result1;
    return result1d;
}
