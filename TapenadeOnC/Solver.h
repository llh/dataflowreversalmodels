#include <stddef.h>

typedef struct _ChainedList{
  double head ;
  struct _ChainedList *tail ;
} ChainedList ;

extern void applyControl(double *du, double *dv) ;

extern double computeCost() ;

extern void ticks(int nbTicks, double dt, double visc, double diff, double ct) ;

extern void tick(double dt, double visc, double diff) ;

extern void vel_step(double* u, double* v, double* u0, double* v0,
              double visc, double dt) ;

extern void dens_step(double* x, double* x0, double* u, double* v,
               double diff, double dt) ;

extern void add_source(double* x, double* s, double dt) ;

extern void diffuse(int b, double diff, double dt) ;

extern void advect(int b, double* d, double* d0, double* u, double* v, double dt) ;

extern void project(double* u, double* v, double* p, double* div) ;

extern void set_bnd(int b, double* x) ;

extern int INDEX(int i, int j) ;
