# DataFlowReversalModels

Test of Data-Flow Reversal strategies for reverse Algorithmic Differentiation, on a small-to-medium size code.

4 strategies tested:
- OO-AD with AdolC (in AdolCOnC++)
- ST-AD with Tapenade for Fortran code (in TapenadeOnFortran)
- ST-AD with Tapenade for C code (in TapenadeOnC)
- Proposed ST-AD model on Java code (in HandCodedOnJava)

Each directory contains an equivalent implementation of a small Navier-Stokes solver, in a different source language.

The `run` script in each directory triggers the test.
